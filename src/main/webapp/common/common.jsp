<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>     
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ECサイト</title>
    <link href="${pageContext.request.contextPath}/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="all">
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.datetimepicker.css">
 	
	<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.pagination.js"></script>


	<script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.datetimepicker.js"></script>
	<script>
	$(function() {
		$('#autocomplete').keyup(function(){
    	   $.getJSON("${pageContext.request.contextPath}/item/autoComplete?name=" + $('#autocomplete').val(),
           function(data) {
    		   $('#autocomplete').autocomplete({
                	source:data,
                	minLength:1
                })
            });
        });
	});
	</script>
  </head>
  <body>
  		<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="${pageContext.request.contextPath}/item/list"> <!-- 企業ロゴ --> <img
						alt="main log" src="${pageContext.request.contextPath}/image/title3.png" height="55">
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					
					<p class="navbar-text navbar-right">
						<c:if test="${user != null && admin == null}"><span>熱くなろうぜ！<c:out value="${user.name}"/>さん&nbsp;&nbsp; </span></c:if>
						<c:if test="${admin == true}"><span class="btn btn-success">管理者モード</span>&nbsp;&nbsp;<c:out value="${user.name}"/>さん&nbsp;&nbsp;</c:if>
						<c:if test="${admin == true}"><a href="${pageContext.request.contextPath}/admin/mypage" class="navbar-link">管理者ページ&nbsp;&nbsp;</a></c:if>
						<a href="${pageContext.request.contextPath}/cart/list" class="navbar-link"><img width=20 height=20 src="${pageContext.request.contextPath}/image/shop.png">カート：
						<c:if test="${cartNum == 0}">0</c:if><c:if test="${cartNum == null}">0</c:if><c:if test="${cartNum != 0 }"><c:out value="${cartNum}"/></c:if></a>&nbsp;&nbsp;
						<c:if test="${user == null}"><a href="${pageContext.request.contextPath}/user/register" class="navbar-link"><img width=20 height=20 src="${pageContext.request.contextPath}/image/登録.png">
						ユーザ登録</a>&nbsp;&nbsp;</c:if>
						<c:if test="${user != null}"><a href="${pageContext.request.contextPath}/user/mypage" class="navbar-link">マイページ</a>&nbsp;&nbsp;</c:if>
						<c:if test="${user == null}"><a href="${pageContext.request.contextPath}/user/login" class="navbar-link"><img width=20 height=20 src="${pageContext.request.contextPath}/image/key.png">
						ログイン</a>&nbsp;&nbsp;</c:if>
						<c:if test="${user != null}"><a href="${pageContext.request.contextPath}/user/logout/sessionInvalidate" class="navbar-link">ログアウト</a></c:if>
					</p>
					
					<!-- 検索フォーム -->
					
					<p><span class="search">
			
					<form:form modelAttribute="itemsForm" action="${pageContext.request.contextPath}/item/search">
					<span class="col-xs-2">
					<form:input path="name" id="autocomplete" class="form-control" />
					</span>
					<input class="btn" type="submit" value="検索">
					</form:form>
					</span>
					</p>
					
					<!-- 検索フォーム 終了-->
					
				</div>
				<!-- /.navbar-collapse -->
			</div>
			    <div class="container" style="padding:10px 0">
 
    	</div>
			<!-- /.container-fluid -->	
		</nav>
		
		<!-- ログイン日時表示 -->
		<c:if test="${user != null}">
		<div class="text-right jumbotront">
		最終ログイン日時：<fmt:formatDate value="${user.loginDate}" pattern="yyyy/MM/dd HH:mm:ss" />
		</div>
		</c:if>
		</div>
  </body>
