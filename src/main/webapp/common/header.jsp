<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <nav class="navbar navbar-default navbar-fixed-top container">
      <div class="navbar-header">
        <a class="navbar-brand" href="${pageContext.request.contextPath}/item/list.html"> 
        <!-- 企業ロゴ --> <img alt="main log" src="${pageContext.request.contextPath}/image/images.jpg" height="35">
		</a>
      </div>
      <div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<p class="navbar-text navbar-right">
					    <c:if test="${user != null}"><img width=20 height=20 src="${pageContext.request.contextPath}/image/hot1.png">
      				 	熱くなろうぜ！<c:out value="${user.name}"/>さん&nbsp;&nbsp;&nbsp;
       					</c:if>
						<a href="${pageContext.request.contextPath}/cart/list"><img width=20 height=20 src="${pageContext.request.contextPath}/image/shop.png">カート：
						<c:if test="${cartNum == 0}">0&nbsp;&nbsp;&nbsp;</c:if><c:if test="${cartNum == null}">0&nbsp;&nbsp;&nbsp;</c:if><c:if test="${cartNum != 0 }"><c:out value="${cartNum}"/>&nbsp;&nbsp;</c:if>
						<c:if test="${user != null}"><a href="${pageContext.request.contextPath}/order/history" class="navbar-link">注文履歴</a>&nbsp;&nbsp;&nbsp;</c:if>
						<c:if test="${user == null}">
	     				<a href="${pageContext.request.contextPath}/user/register" class="navbar-link">ユーザ登録&nbsp;&nbsp;&nbsp;</a>
	    				</c:if>
	    				<c:if test="${user == null}">
						<a href="${pageContext.request.contextPath}/user/login" class="navbar-link">ログイン&nbsp;&nbsp;&nbsp;</a>
						</c:if>
						<c:if test="${user != null}">
						<a href="${pageContext.request.contextPath}/user/logout/sessionInvalidate" class="navbar-link">ログアウト&nbsp;&nbsp;&nbsp;　　</a>
			</c:if>

					</p>
				</div>
      
      <!--  
       <ul class="nav navbar-nav navbar-right">
      
       	<c:if test="${user != null}"> <li><a><img width=20 height=20 src="${pageContext.request.contextPath}/image/hot1.png">
       	熱くなろうぜ！<c:out value="${user.name}"/>さん</a>
       </li>
       </c:if>
        <li>
         <a href="${pageContext.request.contextPath}/cart/list"><img width=20 height=20 src="${pageContext.request.contextPath}/image/shop.png">カート：
         <c:if test="${cartNum == 0}">0</c:if><c:if test="${cartNum == null}">0</c:if><c:if test="${cartNum != 0 }"><c:out value="${cartNum}"/></c:if></a>
        </li>
        <c:if test="${user != null}">
        <li>
         <a href="${pageContext.request.contextPath}/order/history" class="navbar-link">注文履歴</a>
     　　　　　 </li>
    	 </c:if>
    	 <c:if test="${user == null}">
        <li>
	     	<a href="${pageContext.request.contextPath}/user/register" class="navbar-link">ユーザ登録</a>
	    </li>
	    </c:if>
	    <c:if test="${user != null}">
	    <li>
		    <a href="${pageContext.request.contextPath}/user/update" class="navbar-link">ユーザ情報変更</a>
		</li>
		</c:if>
		<c:if test="${user == null}">
		<li>
			<a href="${pageContext.request.contextPath}/user/login" class="navbar-link">ログイン</a>
		</li>
		</c:if>
		<c:if test="${user != null}">
		<li>
			<a href="${pageContext.request.contextPath}/user/logout/sessionInvalidate" class="navbar-link">ログアウト</a>
			</li>
			</c:if>
      </ul>
      -->
 	</nav>
    <div class="container" style="padding:40px 0">
 
    </div>