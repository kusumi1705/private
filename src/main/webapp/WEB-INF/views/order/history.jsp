<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<link rel="stylesheet" href="/css/style.css" media="all">

<body>
<div class="container">
<!--詳細情報-->
<h2>注文履歴画面</h2>
<div>
<h4>注文履歴</h4>
<c:if test="${ordersList.size() == 0}">
	<h5>まだ注文履歴がありません</h5>
</c:if>
<c:forEach var="order" items="${ordersList}">
<c:forEach var="item" items="${order.orderItemsList}">
<div class="img"><img src="${item.imgCode}" width="300px" height="300px"/></div>
<div class="reviewBody">
	<h4>商品名</h4>
	【<c:out value="${item.name}"/>】<br>
	<h4>商品価格</h4>
	<fmt:formatNumber value="${item.price}" pattern="###,###"/>yen<br>
	<h4>評価平均</h4>
　　    <c:if test="${item.scoreAverage > 0}">
    <fmt:formatNumber value="${item.scoreAverage}" maxFractionDigits="2" />
    </c:if>
　    　<c:if test="${item.scoreAverage == 0}">まだレビューがありません
    </c:if>
    <br><br><br><br><br><br>
    <hr>
    <h3>■注文情報</h3>
    注文日：<c:out value="${order.orderDate}"/><br>
    支払い金額：<fmt:formatNumber value="${order.totalPrice}" pattern="###,###"/>yen<br>
    <h4>〇届け先</h4>
        名前:<c:out value="${order.destinationName }"/><br>
        メールアドレス:<c:out value="${order.destinationEmail }"/><br>
        住所:<c:out value="${order.destinationAddress }" /><br>
        電話番号:<c:out value="${order.destinationTel }" /><br>
        支払い方法:<c:if test="${order.paymentMethod == 2}" >クレジットカード支払い</c:if>
    <c:if test="${order.paymentMethod == 1}" >代金引換</c:if>
    <h4>■レビューをする</h4>
    <a href="${pageContext.request.contextPath}/user/review?itemId=${item.id }">レビューはこちら</a>
    <div class="review">
    <br>
    <hr>
    <br>
</div>
</div>
</c:forEach>
</c:forEach>
<br>




</div>
</div>
</body>
</html>