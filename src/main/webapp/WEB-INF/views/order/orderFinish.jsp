<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

		<!-- table -->
		<div class="row">
			<div
				class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
				<h3 class="text-center">ご注文を受けつけました！</h3>
				<p class="text-center">この度はご注文ありがとうございます。</p>
				<p class="text-center">ご注文が届くまでお待ちください。</p>
				<p class="text-center">ご注文を確認する際は、「注文履歴」からご確認ください。</p>
			</div>
		</div>

		<br><br>
		<div class="row">
			<div class="col-xs-offset-5 col-xs-2">
				<div class="form-group">
					<form action="${pageContext.request.contextPath}/item/list">
						<input class="form-control btn btn-warning btn-block"
							type="submit" value="商品一覧画面に戻る">
					</form>
				</div>
			</div>
		</div>
		<br><br><br><br><br><br>

	<!-- end container -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>