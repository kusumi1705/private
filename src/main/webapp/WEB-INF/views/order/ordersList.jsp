<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
	
<div class="row">
	<div class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
		<h3 class="text-center">注文内容確認</h3>
			
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>
							<div class="text-left">
								商品名
							</div>
						</th>
						<th>
							<div class="text-left">
								数量
							</div>
						</th>
						<th>
							<div class="text-center">
								価格
							</div>
						</th>
					</tr>
			
					<c:forEach var="item" items="${itemList}" >
					<tr>
						<td>
							<div class="center">
								<div class="img"><img src="${item.imgCode}" width="200px" height="200px"/></div>
							</div>
						</td>
						<td>	
							【<c:out value="${item.name}"/>】<br>
						</td>
						<td>
							<div class="text-center">
								<fmt:formatNumber value="${item.price}" pattern="###,###"/>yen<br>
								数量<c:out value="${item.quantity}"/><br>
							</div>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

<div class="row">
	<div class="col-xs-offset-2 col-xs-8">
		<div class="form-group text-center">
			合計<fmt:formatNumber value="${total}" pattern="###,###"/>yen<br>
		</div>
	</div>
</div>
		
<form:form modelAttribute="ordersForm" action="${pageContext.request.contextPath}/order/ordersFinish">
<div class="row">
<div class="table-responsive col-lg-offset-3 col-lg-6 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
<h3 class="text-center">お届け先情報</h3>
<table class="table table-striped">
<tbody>
	<tr>
		<td>
			<div class="text-center">
				お名前
			</div>
		</td>
		<td>
		<div class="col-xs-7">
			<form:input class="form-control" path="destinationName"/>
			<form:errors path="destinationName" cssStyle="color:red"/>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="text-center">
				メールアドレス
			</div>
		</td>
		<td>
		<div class="col-xs-7">
			<form:input class="form-control" path="destinationEmail"/>
			<form:errors path="destinationEmail" cssStyle="color:red"/>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="text-center">
				住所
			</div>
		</td>
		<td>
		<div class="col-xs-7">
			<form:input class="form-control" path="destinationAddress"/>
			<form:errors path="destinationAddress" cssStyle="color:red"/>
			</div>
	</tr>
	<tr>
		<td>
			<div class="text-center">
				電話番号
			</div>
		</td>
		<td>
		<div class="col-xs-7">
			<form:input class="form-control" path="destinationTel"/>
			<form:errors path="destinationTel" cssStyle="color:red"/>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="text-center">
				配送日時
			</div>
		</td>
		<td>
	<div class="form-group">
		<div class="row">
			<div class="col-sm-8">
				<label
					class="control-label" style="color: red" for="inputPeriod">配達日時を入力してください
				</label>
			</div>
		<div class="col-sm-5">
			<form:input type="date" path="deliveryDate" id="name" class="form-control input-sm"/>
			<form:errors path="deliveryDate" cssStyle="color:red"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="10:00:00.000" />
					10時
			</label>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="11:00:00.000" /> 
					11時
			</label>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="12:00:00.000" /> 
					12時
			</label><br>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="13:00:00.000" /> 
					13時
			</label>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="14:00:00.000" /> 
					14時
			</label>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="15:00:00.000" /> 
					15時
			</label><br>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="16:00:00.000" /> 
					16時
			</label>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="17:00:00.000" /> 
					17時
			</label>
			<label class="radio-inline"> 
				<form:radiobutton path="deliveryTime" value="18:00:00.000" /> 
					18時
				<form:errors path="deliveryTime" cssStyle="color:red"/>
			</label><br>
		</div>
	</div>
	</div>
	</td>
	</tr>
</tbody>
</table>
</div>
</div>

 		<div class="row"> 
			<div class="table-responsive col-lg-offset-3 col-lg-6 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
				<h3 class="text-center">お支払い方法</h3>
				<table class="table table-striped">
					<tbody>
						<tr>
							<td>
								<div class="text-center">
									代金引換
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-sm-12">
										<label class="radio-inline"> 
											<form:radiobutton path="paymentMethod" value="1" />
											代金引換
										</label>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="text-center">
									クレジットカード決済
								</div>
							</td>
							<td align="center">
								<div class="row">
									<div class="col-sm-12">
										<label class="radio-inline"> 
											<form:radiobutton path="paymentMethod" value="2" checked="checked" />
											クレジットカード
										</label><br><br>
									</div>
								</div>
							 	<script src="https://checkout.webpay.jp/v3/" class="webpay-button" data-key="test_public_0c73EG9wd47F5wXfuSgSK1vZ" data-lang="ja" data-token-name="token" data-partial="true"></script>
								<form:errors path="paymentMethod" cssStyle="color:red"/>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>		 
		<div class="row">
			<div class="col-xs-offset-4 col-xs-4">
				<div class="form-group">
						<input class="form-control btn btn-warning btn-block" type="submit" value="この内容で注文する">
				</div>
			</div>
		</div>
		<br><br><br><br><br><br>			
	<!-- end container -->	 
</form:form>
			
</body>
</html>