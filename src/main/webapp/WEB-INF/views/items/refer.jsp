<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="all">
 <script>
 
	function calc(){
 		document.getElementById("calcOutput").innerHTML =
 			${item.price} * eval(document.priceForm.quantity.value).toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
	}
	
</script>

<body>

	<div class="container">
		<!--詳細情報-->
		<h2>商品詳細</h2>

		<hr>
			<div class="back-button">
				<a href="${pageContext.request.contextPath}/item/list">商品一覧へ戻る</a>
			</div>
			<c:if test="${admin == true}"><a href="${pageContext.request.contextPath}/admin/updateItem?id=${item.id}" class="btn btn-info">商品情報編集</a></c:if>
			<c:if test="${admin == true}"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">削除確認画面</button></c:if>
					<div class="modal fade" id="myModal">
  					<div class="modal-dialog">
    				<div class="modal-content">
      				<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title">削除確認画面</h4>
     				</div>
      				<div class="modal-body">削除確認です。本当に削除しますか？</div>
      				<div class="modal-footer">
      				<a href="${pageContext.request.contextPath}/admin/deleteItemAction?id=${item.id}" class="btn btn-danger">商品削除実行</a>
        			<button type="button" class="btn btn-primary" data-dismiss="modal">やっぱりやめる</button>
       				</div>
    				</div>
  					</div>
					</div>
		    <div class="image-box">
				<h3>【商品画像】</h3>
				<div class="img"><img src="${item.imgCode}" width="350px" height="350px"/></div>
			</div>
			<div class="refer">
				<h4>【商品名】</h4>
				<c:out value="${item.name}"/><br>
				<h4>【商品価格】</h4>
				<fmt:formatNumber value="${item.price}" pattern="###,###"/>yen<br>
				<h4>【評価平均】</h4>
			　　   	 <c:if test="${item.scoreAverage > 0}">
   					<a href="${pageContext.request.contextPath}/item/review?itemId=${item.id}"><c:out value="${item.scoreSymbol}"/>&nbsp;&nbsp;<fmt:formatNumber value="${item.scoreAverage}" maxFractionDigits="2" />
   						&nbsp;&nbsp;&nbsp;レビュー詳細はこちら
   					</a>
   				</c:if>
　    　			<c:if test="${item.scoreAverage == 0}">まだレビューがありません</c:if>

				<!-- 個数選択等　 -->
				<form:form modelAttribute="shoppingCartForm" action="${pageContext.request.contextPath}/cart/addlist" name="priceForm">
					<div class="goCart">
						数量：<select name="quantity" onChange="calc()">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>個
						金額：<span id="calcOutput"><script>calc();</script></span>yen
							<input type="submit" class="btn btn-primary" value="カートに入れる">
							<input type="hidden" name="itemId" value="${item.id}">
					</div>
			
				</form:form>	
			</div>
<hr>
			<h4>【商品説明】</h4>
			<div class="description">
				<c:out value="${item.description}"/><br>
			</div>
					
	</div>

</body>
</html>