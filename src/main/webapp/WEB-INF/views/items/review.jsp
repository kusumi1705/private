<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="all">

<body>
<div class="container">
<h2>レビュー画面</h2>
<hr>
<div>
<h3>商品画像</h3>
<div class="img"><img src="${item.imgCode}" width="300px" height="300px"/></div>
<div class="reviewBody">
	<h4>商品名</h4>
	【<c:out value="${item.name}"/>】<br>
	<h4>商品価格</h4>
	<fmt:formatNumber value="${item.price}" pattern="###,###"/>yen<br>
	<h4>評価平均</h4>
　　    <c:if test="${item.scoreAverage > 0}">
    <c:out value="${item.scoreSymbol}"/>&nbsp;&nbsp;<fmt:formatNumber value="${item.scoreAverage}" maxFractionDigits="2" />
    </c:if>
　    　<c:if test="${item.scoreAverage == 0}">まだレビューがありません
    </c:if>
</div>
<br>
<div class="description">
<h4>商品説明</h4>
<c:out value="${item.description}"/><br>
</div>
<div class="review">
<br>
<c:forEach var="review" items="${item.reviewList}">
<hr>
<h5>評価タイトル</h5>
<c:out value="${review.title}"/>
<h5>評価者</h5>
<c:out value="${review.reviewer}"/>
<h5>評価</h5>
<c:out value="${review.scoreSymbol}"/>&nbsp;&nbsp;<c:out value="${review.score}" />
<h5></h5>
<c:out value="${review.postDate}"/>
<h5>評価コメント</h5>
<c:out value="${review.comment }"/>
</c:forEach>

</div>



</div>
</div>
</body>
</html>