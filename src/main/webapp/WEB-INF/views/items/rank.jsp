<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="all">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
 <script>
 $(function() {
	    $.getJSON('${pageContext.request.contextPath}/item/getRankList', function(rankList) {
	        console.log(rankList);
	        var contextPath = $("#contextPath").val();
	        var html;
	        var labels = [], data=[];
	        $.each(rankList, function(i, item){
	        	if(i >= 10){
	        		return false;
	        	}
	        	labels.push(item.name);
	        	data.push(item.num);
	        });
	        
	     var barChartData = {
	        		  labels : labels,
	        		  datasets:[
	        		    {
	        		      label: "売上個数",
	        		      fillColor : /*"#d685b0"*/"rgba(179,181,198,1)",
	        		      strokeColor : /*"#d685b0"*/"rgba(179,181,198,0.4)",
	        		      highlightFill: /*"#eebdcb"*/"rgba(238,189,203,0.7)",
	        		      highlightStroke: /*"#eebdcb"*/"rgba(238,189,203,0.7)",
	        		      data : data
	        		    }]
	        		}
	    
	    console.log(barChartData)
	    var ctx = document.getElementById("chart").getContext("2d");
	    window.myBar = new Chart(ctx).Bar(barChartData, {
	      responsive : true,
	      // アニメーションを停止させる場合は下記を追加
	      /* animation : false */
	    });
	    }); 
	});

</script>

<body>
	<div class="container">
		<!--詳細情報-->
		<h2>商品売り上げ個数TOP10ランキング</h2>
		<h4>■縦軸が売り上げ個数、横軸は商品名</h4>
		<hr>
	<div style="width: 70%">
    <canvas id="chart" height="450" width="600"></canvas>
    </div>
	</div>
</body>
</html>