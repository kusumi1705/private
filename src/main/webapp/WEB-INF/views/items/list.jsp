<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="all">
<link type="text/css" rel="stylesheet"
  href="http://code.jquery.com/ui/1.10.3/themes/cupertino/jquery-ui.min.css" />
<script type="text/javascript"
  src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
  src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<link type="text/css" rel="stylesheet"
  href="http://code.jquery.com/ui/1.10.3/themes/cupertino/jquery-ui.min.css" />
<script type="text/javascript"
  src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
  src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.pagination.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript">

$(function(){
	var carouObj = new Object();
	carouObj.auto = 1000;
	carouObj.scroll = {
		duration:3000,
		pauseOnHover:"immediate"
	};
	$(".thumb-wrapper ul").carouFredSel(carouObj);
});


$(function() {
    var showFlag = false;
    var topBtn = $('#pageTop');    
    topBtn.css('bottom', '-100px');
    var showFlag = false;
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            if (showFlag == false) {
                showFlag = true;
                topBtn.stop().animate({'bottom' : '20px'}, 200); 
            }
        } else {
            if (showFlag) {
                showFlag = false;
                topBtn.stop().animate({'bottom' : '-100px'}, 200); 
            }
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});

</script>
<style>
	.thumb-wrapper {
		margin:20px;
		padding: 10px 20px 0px 0px;
		width:1140px;
		overflow:hidden;
	}
	
	.thumb-wrapper {
		margin:0;
		padding:0;
	}
	
	.thumb-wrapper ul li{
		list-style:none;
		display:block;
		float:left;
	}
	
	.thumb-wrapper li img {
		margin:5px 50px 1px 50px;
	}

</style>

<div class="container">
<div id="wrapper">
<header id="headerContainer">
		<hr class="big"/>

	<div class="thumb-wrapper">
		<ul>
			<li><a href="${pageContext.request.contextPath}/item/search?name=puma"><img src="${pageContext.request.contextPath}/image/PUMA.png" alt="イラスト２" width=60 height=60></a></li>
			<li><a href="${pageContext.request.contextPath}/item/search?name=アディダス"><img src="${pageContext.request.contextPath}/image/addidas.png" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=NIKE"><img src="${pageContext.request.contextPath}/image/NIKE.png" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=千葉ロッテ"><img src="${pageContext.request.contextPath}/image/千葉ロッテ.jpg" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=mizuno"><img src="${pageContext.request.contextPath}/image/mizuno.png" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=ナイキ"><img src="${pageContext.request.contextPath}/image/ナイキ.jpg" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=yonex"><img src="${pageContext.request.contextPath}/image/yonex.png" alt="イラスト２" width=60 height=60></a></li>
	  		<li><a href="${pageContext.request.contextPath}/item/search?name=puma"><img src="${pageContext.request.contextPath}/image/PUMA.png" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=アディダス"><img src="${pageContext.request.contextPath}/image/addidas.png" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=NIKE"><img src="${pageContext.request.contextPath}/image/NIKE.png" alt="イラスト２" width=60 height=60></a></li>
	 		<li><a href="${pageContext.request.contextPath}/item/search?name=長嶋監督"><img src="${pageContext.request.contextPath}/image/長嶋監督.jpg" alt="イラスト２" width=60 height=60></a></li>
		 </ul>
	 </div>
				

<script> 

$(function() {
    $.getJSON($("#contextPath").val()+'/item/jsonItemsList?page=1', function(itemList) {
        console.log(itemList);
        var contextPath = $("#contextPath").val();
        var html;
        $.each(itemList, function(i, item){
                html = '<div class="view"><div class="innerView"><div class="text-center"><br>'
                    + '<a href='+ contextPath + '/item/refer?id=' + item.id +'><img src='+ item.imgCode +' width="200px" height="200px"/></a><br>'
                    + '<span class="itemTitle"><a href='+ contextPath + '/item/refer?id=' + item.id +'>' + item.name + '</a><br>'
                    + '<img alt="icon" src="' + contextPath + '/image/money.png" height="25">&nbsp;&nbsp;：' + Number(item.price).toLocaleString() + ' yen<br>';
                    
                    if(item.scoreSymbol != null){
                    	html += '<a href="${pageContext.request.contextPath}/item/review?itemId=$' + item.id + '>'
                    	     + '<img alt="icon" src="' + contextPath + '/image/評価.png" height="25">&nbsp;&nbsp;評価&nbsp;&nbsp; ' + item.scoreSymbol + item.scoreAverage + '</a>';
                    }
                html += '</span></div></div></div>';
            $("#list2").append(html);
        });
    });
});

$(function() {
	$("#pagerDiv").on('click', ".getJsonByPage", function(){
		
	var num;
		
	if(this.innerHTML == "最初に戻る"){
		num = 1;
	}else if(this.innerHTML == "最後に進む"){
	    num = ${pageNums.size()};
	}else{
		num = Number(this.innerHTML);
	}
	
    $.getJSON($("#contextPath").val()+'/item/jsonItemsList?page=' + num, function(itemList) {
        $("#list2").html("");
        var contextPath = $("#contextPath").val();
        var html;
        $.each(itemList, function(i, item){
                html = '<div class="view"><div class="innerView"><div class="text-center"><br>'
                    + '<a href='+ contextPath + '/item/refer?id=' + item.id +'><img src='+ item.imgCode +' width="200px" height="200px"/></a><br>'
                    + '<span class="itemTitle"><a href='+ contextPath + '/item/refer?id=' + item.id +'>' + item.name + '</a><br>'
                    + '<img alt="icon" src="' + contextPath + '/image/money.png" height="25">&nbsp;&nbsp;：' + Number(item.price).toLocaleString() + ' yen<br>';
                    
                    if(item.scoreSymbol != null){
                    	html += '<a href="${pageContext.request.contextPath}/item/review?itemId=$' + item.id + '>'
                    	     + '<img alt="icon" src="' + contextPath + '/image/評価.png" height="25">&nbsp;&nbsp;評価&nbsp;&nbsp; ' + item.scoreSymbol + item.scoreAverage + '</a>';
                    }
                html += '</span></div></div></div>';
            $("#list2").append(html);
        });
        
        $.getJSON($("#contextPath").val()+'/item/jsonGetPager?page=' + num, function(pageNums) {
        		$("#pagerDiv").html("");
        		
        		var contextPath = $("#contextPath").val();
        		var html ='<ul class="pagination">'
			         + '<li><a href="#" class="getJsonByPage">最初に戻る</a></li>'
			         + '<li><a href="#">«</a></li>';
			         
        		for(var i = 0; i < pageNums.length; i++){
        			if(pageNums[i] == 0){
        				html += '<li style="pointer-events:none;"><a href="#">...</a></li>';
        			}else if(pageNums[i] == num){
        				html += '<li class="active"><a href="#" class="getJsonByPage">' + pageNums[i] + '</a></li>';
        			}else if(pageNums[i] != null){
        			    html += '<li><a href="#" class="getJsonByPage">' + pageNums[i] + '</a></li>';
        			}
                }
           		
        		html += '<li><a href="#">»</a></li>'
      			     +    '<li><a href="#" class="getJsonByPage">最後に進む</a></li></ul>';
           		$("#pagerDiv").append(html);
            });
        
        });
    });
});


$(function() {
    // Ajax通信テスト ボタンクリック
    $("#ajax_btn").click(function() {
        // outputDataを空に初期化
        $("#output_data").text("");
        $.ajax({
            type        : "GET",
            url         : "getTestData",
            dataType    : "json",
            success     : function(data) {
                            success(data);
                        },
            error       : function(XMLHttpRequest, textStatus, errorThrown) {
                            error(XMLHttpRequest, textStatus, errorThrown);
                        }
        });
    });
});
 
// Ajax通信成功時処理
function success(data) {
    alert("success:" + data);
 
    $("#output_data").text("");
    for (var cnt = 0; cnt < data.length; cnt++) {
        $("#output_data").append("data[" + cnt + "]：" + data[cnt] + "；");
    }
}
 
// Ajax通信失敗時処理
function error(XMLHttpRequest, textStatus, errorThrown) {
    alert("error:" + XMLHttpRequest);
    alert("status:" + textStatus);
    alert("errorThrown:" + errorThrown);
}
</script>
<hr class="big">
<h2>【商品一覧】</h2>
<input id="contextPath" type="hidden" value="${pageContext.request.contextPath}"/>

<div style="float:right;">
商品の表示数を変更する
<form action="${pageContext.request.contextPath}/item/diplayItemNum">
<select name="displayNum">
<option value="9" selected></option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">初期値に戻す</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="15">15</option>
<option value="18">18</option>
</select>
<input type="submit" class="btn btn-default" value="変更">
</form>
<a href="${pageContext.request.contextPath}/item/rank">商品ランキングを見る</a>
</div>
<div style="clear:right;"></div>
<span class="text-success text-left"><c:out value="${adminMessage}"/></span>
<div class="list">
	<c:if test="${fn:length(itemList) == 0 }">
		<div class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
			<h3 class="text-center">該当する商品がありません</h3>
		</div>
	</c:if>
<%-- <c:forEach var="items" varStatus="status" items="${itemList}"> --%>
<!-- <div class="view"> -->
<!-- <div class="innerView"> -->
<!-- <div class="text-center"> -->
<!-- <br> -->
<%-- <a href="${pageContext.request.contextPath}/item/refer?id=${items.id }" ><img src="${items.imgCode}" width="200px" height="200px"/></a><br> --%>
<!-- <span class="itemTitle"> -->
<%-- <a href="${pageContext.request.contextPath}/item/refer?id=${items.id }"><c:out value="${items.name}"/></a><br> --%>
<%-- <img alt="icon" src="${pageContext.request.contextPath}/image/money.png" height="25">&nbsp;&nbsp;：<fmt:formatNumber value="${items.price}" pattern="###,###"/>yen<br> --%>
<%-- <c:if test="${items.scoreAverage > 0}"> --%>
<%-- <a href="${pageContext.request.contextPath}/item/review?itemId=${items.id}"> --%>
<%-- <img alt="icon" src="${pageContext.request.contextPath}/image/評価.png" height="25">&nbsp;&nbsp;評価&nbsp;&nbsp;<c:out value="${items.scoreSymbol}" /> <fmt:formatNumber value="${items.scoreAverage}" maxFractionDigits="2" /></a> --%>
<!-- </span> -->
<%-- </c:if> --%>
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->
<%-- </c:forEach> --%>

<div id="list2">
</div>

	<div class="pager"></div>
</div>
</div>
<p id="pageTop"><a href="#wrapper">PAGE TOP</a></p>
</header>

<div id="pagerDiv" style="text-align:center;">
<ul class="pagination">
  <li><a href="#" class="getJsonByPage">最初に戻る</a></li>
  <li <c:if test="${pageNum == 1}">style="pointer-events:none;"  </c:if>><a href="${pageContext.request.contextPath}/item/page?page=${pageNum - 1}">«</a></li>
  <c:forEach var="num" items="${pageNums}" varStatus="dataNum">
  <c:if test="${pageNum <= dataNum.count + 2 && pageNum >= dataNum.count - 2 && pageNums.size() - 2  > dataNum.count}">
  <li <c:if test="${pageNum == dataNum.count}">class="active" </c:if> ><a href="#" class="getJsonByPage"><c:out value="${num}"/></a></li>
  </c:if>
  <c:if test="${pageNum + 4 == dataNum.count}">
  <li style="pointer-events:none;"><a href="#">...</a></li>
  </c:if>
  <c:if test="${pageNums.size() - 2 <= dataNum.count}">
  <li <c:if test="${pageNum == dataNum.count}">class="active" </c:if> ><a href="#" class="getJsonByPage"><c:out value="${num}"/></a></li>
  </c:if>
  </c:forEach>
  <li <c:if test="${pageNum == 1}">style="pointer-events:none;"  </c:if>><a href="${pageContext.request.contextPath}/item/page?page=${pageNum - 1}">»</a></li>
 　<li><a href="#" class="getJsonByPage">最後に進む</a></li>
</ul>
</div>


<!-- ぺージャー表示部分 -->

<!-- <div style="text-align:center;"> -->
<!-- <ul class="pagination"> -->
<%--   <li><a href="${pageContext.request.contextPath}/item/page?page=1">最初に戻る</a></li> --%>
<%--   <li <c:if test="${pageNum == 1}">style="pointer-events:none;"  </c:if>><a href="${pageContext.request.contextPath}/item/page?page=${pageNum - 1}">«</a></li> --%>
<%--   <c:forEach var="num" items="${pageNums}" varStatus="dataNum"> --%>
<%--   <c:if test="${pageNum <= dataNum.count + 2 && pageNum >= dataNum.count - 2 && pageNums.size() - 2  > dataNum.count}"> --%>
<%--   <li <c:if test="${pageNum == dataNum.count}">class="active" </c:if> ><a href="${pageContext.request.contextPath}/item/page?page=${num}"><c:out value="${num}"/></a></li> --%>
<%--   </c:if> --%>
<%--   <c:if test="${pageNum + 4 == dataNum.count}"> --%>
<!--   <li style="pointer-events:none;"><a href="#">...</a></li> -->
<%--   </c:if> --%>
<%--   <c:if test="${ pageNums.size() - 2 <= dataNum.count}"> --%>
<%--   <li <c:if test="${pageNum == dataNum.count}">class="active" </c:if> ><a href="${pageContext.request.contextPath}/item/page?page=${num}"><c:out value="${num}"/></a></li> --%>
<%--   </c:if> --%>
  
<%--   </c:forEach> --%>
<%--   <li <c:if test="${pageNum.equals(pageNums.size())}">style="pointer-events:none;"</c:if>><a href="${pageContext.request.contextPath}/item/page?page=${pageNum + 1}">»</a></li> --%>
<%--  　<li><a href="${pageContext.request.contextPath}/item/page?page=${pageNums.size()}">最後に進む</a></li> --%>
<!-- </ul> -->
<!-- </div> -->

<!-- ぺージャー表示部分 終了-->

</div>
</body>
</html>