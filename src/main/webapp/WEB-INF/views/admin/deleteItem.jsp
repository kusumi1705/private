<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>

  <body>
    <div class="container">
	<h3>商品情報削除画面</h3>
	<div class="span8">
		<div class="row">
		<form:form modelAttribute="ItemForm" action="${pageContext.request.contextPath}/admin/deleteItemAction" method="POST">
		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">一括削除確認画面</button>
		<!-- モーダルウィンドウの中身 -->
		<div class="modal fade" id="myModal">
  			<div class="modal-dialog">
    			<div class="modal-content">
      			<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">削除確認画面</h4>
     			</div>
      			<div class="modal-body">削除確認です。本当に全件削除しますか？</div>
      			<div class="modal-footer">
      			<a href="${pageContext.request.contextPath}/admin/deleteAllItemAction" class="btn btn-danger">一括削除実行</a>
        		<button type="button" class="btn btn-primary" data-dismiss="modal">やっぱりやめる</button>
       			</div>
    			</div>
  			</div>
		</div>
		<h3>商品一覧</h3>
		<table class="table table-striped">			
			  <tr>
			    <th>
			    	商品ID
			    </th>
			    <th>
			     	 商品画像
			    </th>
			    <th>
			      	商品名
			    </th>
			    <th>
			    	値段
			  　　 </th>
			    <th>
			      	商品説明
			    </th>
			 </tr>
			 <c:forEach var="item" items="${itemList}">
			 <tr>
			 	<th>
			 	<c:out value="${item.id}"/>
			 	    <input type="checkbox" id="check${item.id}" name="updateId" value="${item.id}">
			 	</th>
			  	 <th>
			     	<img src="${item.imgCode}" width="100px" height="100px"/>
			    </th>
			    <th>
			      	<c:out value="${item.name}"/>
			    </th>
			    <th>
			    	<fmt:formatNumber value="${item.price}" pattern="###,###"/>yen
			  　　 </th>
			    <th>
			      	<div class="description">
						<c:out value="${item.description}"/><br>
					</div>
			    </th>
			  </tr>
			</c:forEach>
			  <tr>
			  	<td></td>
			    <td>
			    	<input type="hidden" value="${item.id}" name = "id"/>
					<input class="btn" type="submit" value="編集">
			    </td>
			  </tr>
			</table>
		  </form:form>
		</div>
	</div>
	<br>
	<br>
	<br>
</div>
<script>
$('#upfile').change(function(){
  if (this.files.length > 0) {
    // 選択されたファイル情報を取得
    var file = this.files[0];
    
    // readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
    var reader = new FileReader();
    reader.readAsDataURL(file);
    
    reader.onload = function() {
      $('#showImage').attr('src', reader.result );
      $('#imgSrc').attr('value', reader.result );
    }
  }
});
</script>

  </body>
</html>