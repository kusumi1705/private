<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>

  <body>
    <div class="container">
	<h3>商品情報追加画面</h3>
	<div class="span8">
		<div class="row">
		<form:form modelAttribute="itemsForm" action="${pageContext.request.contextPath}/admin/insertAllItemAction" enctype="multipart/form-data" method="POST">
		<p>商品情報一括取り込み(CSV)</p>
		<input type="file" class="btn btn-default" name="file" accept="text/comma-separated-values"/>
		<input class="btn" type="submit" value="取り込み実行"><br>
		<a href="${pageContext.request.contextPath}/test.csv" download="test.csv"> CSVをダウンロードする</a>
		</form:form>
		<form:form modelAttribute="InsertedAllItemsForm" action="${pageContext.request.contextPath}/admin/insertedAllItemsAction" enctype="multipart/form-data" method="POST">
		<br>
		
		<table class="table table-striped">
			  <tr>
			    <th>
			      	商品名
			    </th>
			    <th>
			    	値段
			  　　 </th>
			    <th>
			      	商品説明
			    </th>
			     <th>
			     	 商品画像
			    </th>
			  </tr>
			  <c:forEach var="item" items="${itemList}" varStatus="num">
			  <tr>
			    <td>
			    	<c:out value="${item.name}"/><br><form:errors path="name" cssStyle="color:red"/>
			    	<input type="hidden" value="${item.name}" name="name">
			    </td>
			    <td>
					<fmt:formatNumber value="${item.price}" pattern="###,###"/>yen
					<input type="hidden" value="${item.price}" name="price">
			    </td>
			    <td>
					<c:out value="${item.description}"/>
					<input type="hidden" value="${item.description}" name="description">
			    </td>
			    <td>
			     	 <h5>画像</h5>
			     	 <input type="file" class="btn btn-default" id="upfile${num.index}" accept="image/jpeg, image/gif, image/png"/>
			 　　　　　　  <img id="showImage" src="" style="width:200px;heght:200px;">
			     	 <input type="hidden" name="imgSrcs" id="imgSrc" value="">
			    </td>
			  </tr>
			  </c:forEach>
			  <tr>
			  	<td></td>
			    <td>
			    	<input type="hidden" value="${item.id}" name = "id"/>
					<input class="btn" type="submit" value="登録">
			    </td>
			  </tr>
			</table>
		  </form:form>
		</div>
	</div>
	<br>
	<br>
	<br>
</div>
<script>
//$('#upfile').change(function(){
$('input').change(function(){
  if (this.files.length > 0) {
    // 選択されたファイル情報を取得
    var file = this.files[0];
    
    // readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
    var reader = new FileReader();
    reader.readAsDataURL(file);
    var position = this;
    
    reader.onload = function() {
      $(position).next().attr('src', reader.result);
      //$('#showImage').attr('src', reader.result );
      $(position).next().next().attr('value', reader.result );
    }
  }
});
</script>

  </body>
</html>