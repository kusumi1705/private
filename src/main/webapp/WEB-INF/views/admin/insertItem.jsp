<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>

  <body>
    <div class="container">
	<h3>商品情報追加画面</h3>
	<div class="span8">
		<div class="row">
		<form:form modelAttribute="itemsForm" action="${pageContext.request.contextPath}/admin/insertItemAction" enctype="multipart/form-data" method="POST">
		<table class="table table-striped">			
			  <tr>
			    <th>
			      	商品名
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<input type="text" name="name" class="form-control" /><br><form:errors path="name" cssStyle="color:red"/>
			    </div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			    	値段
			  　　 </th>
			    <td>
			    <div class="col-xs-6">
					<input class="form-control" type="text" name="price" /><br><form:errors path="price" cssStyle="color:red"/>
				</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			     	 商品画像
			    </th>
			    <td>
			     	 <input type="file" class="btn btn-default" id="upfile" accept="image/jpeg, image/gif, image/png"/>
			     	 <br>
			 　　　　　　  <img id="showImage" src="">
			     	 <input type="hidden" name="imgCode" id="imgSrc" value="">
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	商品説明
			    </th>
			    <td>
			    	<textarea class="form-control" name="description" cols=40 rows=8></textarea><form:errors path="description" cssStyle="color:red"/>
			    </td>
			  </tr>
	
			  <tr>
			  	<td></td>
			    <td>
					<input class="btn" type="submit" value="登録">
			    </td>
			  </tr>
			</table>
		  </form:form>
		</div>
	</div>
	<br>
	<br>
	<br>
</div>
<script>
$('#upfile').change(function(){
  if (this.files.length > 0) {
    // 選択されたファイル情報を取得
    var file = this.files[0];
    
    // readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
    var reader = new FileReader();
    reader.readAsDataURL(file);
    
    reader.onload = function() {
      $('#showImage').attr('src', reader.result );
      $('#imgSrc').attr('value', reader.result );
    }
  }
});
</script>

  </body>
</html>