<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>管理者ページ画面</h3>
	<span class="text-success text-left"><c:out value="${adminMessage}"/></span>
	<div class="span8">
		<div class="row">
		
			<table class="table table-striped">
			　　<tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/admin/updateUsers">会員情報表示・変更・退会処理・一括CSVダウンロード</a>
			    </div>
			    </th>
			  </tr>
			  <tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/admin/ordersHistory">注文履歴・注文履歴一括CSVダウンロード</a>
			    </div>
			    </th>
			  </tr>
			  <tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/admin/insertItem">商品情報追加</a>
			    </div>
			    </th>
			  </tr>
			  <tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/admin/insertAllItem">商品情報一括追加</a>
			    </div>
			    </th>
			  </tr>
			  <tr>
			  <tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/admin/updateAllItem">商品情報一括変更</a>
			    </div>
			    </th>
			  </tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/admin/deleteItem">商品情報削除・一括削除</a>
			    </div>
			    </th>
			  </tr>	
			  <tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/user/update">ユーザ情報変更</a>
			    </div>
			    </th>
			  </tr>
			  <tr>
			  	<th>
			  	 <div class="text-center">
			  		<a href="${pageContext.request.contextPath}/order/history">注文履歴</a>	
			  	</div>
			  	</th>
			  </tr>
			  <tr>
			</table>
		</div>
	</div>
</div>


  </body>
</html>