<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>注文履歴画面</h3>
	<h4>注文履歴情報</h4>
	<button type="button" onclick="location.href='${pageContext.request.contextPath}/admin/ordersCsvDownload'" class="btn btn-primary">
	注文履歴情報一括ダウンロード(csv)</button>
	<span class="text-success text-left"><c:out value="${adminMessage}"/></span>
	<div class="span8">
		<div class="row">
		
			<table class="table table-striped">
			<c:forEach var="order" items="${orderList}">
			　　<tr>
			    <th>
					ID
			    </th>
			    <th>
					ユーザID
			    </th>
			    <th>
					注文状況
			    </th>
			    <th>
					注文合計金額
			    </th>
			   　　<th>
					注文日
			    </th>
			    <th>
					届け先名
			    </th>
			    <th>
					届け先メールアドレス
			    </th>
			    <th>
					届け先住所
			    </th>
			    <th>
					届け先電話番号
			    </th>
			    <th>
			    	配達時間
			    </th>
			    <th>
			    	支払い方法
			    </th>
			    <th>
			    	注文ID
			    </th>
			    <th>
			  </tr>
			  <tr>
				<td>
					<c:out value="${order.id}"/>
				</td>
				<td>
					<c:out value="${order.userId}"/>
				</td>
				<td>
					<c:out value="${order.status}"/>
				</td>
				<td>
					<fmt:formatNumber value="${order.totalPrice}" pattern="###,###"/>
				</td>
				<td>
					<c:out value="${order.orderDate}"/>
				</td>
				<td>
					<c:out value="${order.destinationName}" />
				</td>
				<td>
					<c:out value="${order.destinationEmail}" />
				</td>
				<td>
					<c:out value="${order.destinationAddress}" />
				</td>
				<td>
					<c:out value="${order.destinationTel}" />
				</td>
				<td>
					<c:out value="${order.deliveryTime}" />
				</td>
				<td>
					<c:out value="${order.paymentMethod}" />
				</td>
				<td>
					<c:out value="${order.orderItemsId}" />
				</td>
			  </tr>	
			  </c:forEach>
			</table>
		</div>
	</div>
</div>


  </body>
</html>