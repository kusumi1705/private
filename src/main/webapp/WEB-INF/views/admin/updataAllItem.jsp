<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>

  <body>
    <div class="container">
	<h3>商品情報変更画面</h3>
	<p>■変更したい内容を下記フォームに入力してください</p>
	<p>■全件一括で変更したい場合は、全件変更処理ボタンを押してください</p>
	<p>■複数の商品だけ変更したい場合は、下記商品一覧で変更したい商品にチェックボックスを入れて選択商品変更処理を押してください</p>
	<p>■下記商品一覧の既存の文章や画像を使いたい場合は、コピーボタンを押すとフォームにコピーされます。</p>
	<div class="span8">
		<div class="row">
		<form:form modelAttribute="itemsForm" action="${pageContext.request.contextPath}/admin/updateAllItemAction" method="POST">
		<table class="table table-striped">			
			  <tr>
			    <th>
			      	商品名
			    </th>
			    <td>
			    <div class="col-xs-4">
			    	<input type="text" id="updateName" name="name" value="" class="form-control" /><br><form:errors path="name" cssStyle="color:red"/>
			    </div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			    	値段
			  　　 </th>
			    <td>
			    <div class="col-xs-4">
					<input class="form-control" id="updatePrice" type="text" name="price" value="${item.price}" /><br><form:errors path="price" cssStyle="color:red"/>
				</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			     	 商品画像
			    </th>
			    <td>
			     	 <h5>新しい画像</h5>
			     	 <input type="file" class="btn btn-default" id="upfile" accept="image/jpeg, image/gif, image/png"/>
			 　　　　　　  <img id="showImage" src="" width="100px" height="100px" ">
			     	 <input type="hidden" name="imgCode" id="imgSrc" value="">
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	商品説明
			    </th>
			    <td>
			    	<textarea class="form-control" name="description" id="updateDescription" cols=40 rows=8><c:out value="${item.description}"/></textarea><form:errors path="description" cssStyle="color:red"/>
			    </td>
			  </tr>
			  <tr>
			  	<td></td>
			    <td>
					<input class="btn" type="submit" id="submitAll" value="全件変更処理">
					<input class="btn" type="submit" value="選択商品変更処理">
			    </td>
			  </tr>
	    </table>

		<br>
		<h2>商品一覧情報</h2>
		<br>
		
		<table class="table table-striped">			
			  <tr>
			    <th>
			    	商品ID
			    </th>
			    <th>
			     	 商品画像
			    </th>
			    <th>
			      	商品名
			    </th>
			    <th>
			    	値段
			  　　 </th>
			    <th>
			      	商品説明
			    </th>
			 </tr>
			 <c:forEach var="item" items="${itemList}">
			 <tr>
			 	<th>
			 	<c:out value="${item.id}"/>
			 	    <input type="checkbox" id="check${item.id}" name="updateId" value="${item.id}">
			 	</th>
			 	<th>
			     	<img src="${item.imgCode}" id="imgCode" width="100px" height="100px"/>
			     	<button style="width:50px;height:25px;font-size:10px;text-align:center;" type="button" id="copyImgCode${item.id}" class="btn btn-info">コピー</button>
			    </th>
			    <th>
			      	<div id="name"><c:out value="${item.name}"/></div>
			      	<button style="width:50px;height:25px;font-size:10px;text-align:center;" type="button" id="copyName${item.id}" class="btn btn-info">コピー</button>
			    </th>
			    <th>
			    	<div id="price"><fmt:formatNumber value="${item.price}" pattern="###,###"/></div>yen
			    	<button style="width:50px;height:25px;font-size:10px;text-align:center;" type="button" id="copyPrice${item.id}" class="btn btn-info">コピー</button>
			  　　 </th>
			    <th>
			      	<div class="col-xs-8">
						<div id="description"><c:out value="${item.description}"/></div>
						<button style="width:50px;height:25px;font-size:10px;text-align:center;" type="button" id="copyDescription${item.id}" class="btn btn-info">コピー</button>
					</div>
			    </th>
			  </tr>
			</c:forEach>
			</table>
		  </form:form>
		</div>
	</div>
	<br>
	<br>
	<br>
</div>
<script>
$('button[id^=copyName]').click(function(){
	$('#updateName').val($(this).prev('#name').text());
});

$('button[id^=copyDescription]').click(function(){
	$('#updateDescription').val($(this).prev('#description').text());
});

$('button[id^=copyPrice]').click(function(){
	$('#updatePrice').val($(this).prev('#price').text().replace(",", ""));
});

$('button[id^=copyImgCode]').click(function(){
	$('#imgSrc').val($(this).prev('#imgCode').attr("src"));
	$('#showImage').attr('src', $(this).prev('#imgCode').attr("src"));
});

$(function() {
	$('input[id^=check]').click(function() {
		console.log($('input:checked').length);
		if ($('input:checked').length > 0) {
			$('#submitAll').attr('disabled', 'disabled');
		} else {
			$('#submitAll').removeAttr('disabled');
		}
	});
});


$('#upfile').change(function(){
  if (this.files.length > 0) {
    // 選択されたファイル情報を取得
    var file = this.files[0];
    
    // readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
    var reader = new FileReader();
    reader.readAsDataURL(file);
    
    reader.onload = function() {
      $('#showImage').attr('src', reader.result );
      $('#imgSrc').attr('value', reader.result );
    }
  }
});
</script>

  </body>
</html>