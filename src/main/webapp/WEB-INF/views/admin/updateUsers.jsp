<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>会員情報変更画面</h3>
	<h4>会員情報</h4>
	<button type="button" onclick="location.href='${pageContext.request.contextPath}/admin/userCsvDownload'" class="btn btn-primary">
	ユーザ情報一括ダウンロード(csv)</button>
	<span class="text-success text-left"><c:out value="${adminMessage}"/></span>
	※ユーザ変更処理画面はまだ未実装。
	<div class="span8">
		<div class="row">
		
			<table class="table table-striped">
			<c:forEach var="user" items="${userList}">
			　　<tr>
			    <th>
			    　　	ユーザID
			    </th>
			    <th>
			    　　	名前
			    </th>
			    <th>
			    　　	メールアドレス
			    </th>
			    <th>
			    　　	住所
			    </th>
			   　　<th>
			    　　	電話番号
			    </th>
			    <th>
			               最終ログイン日時
			    </th>
			    <th>
			    	退会処理
			    </th>
			    <th>
			    	変更処理
			    </th>
			    <th>
			  </tr>
			  <tr>
				<td>
					<c:out value="${user.id }"/>
				</td>
				<td>
					<c:out value="${user.name}"/>
				</td>
				<td>
					<c:out value="${user.email}"/>
				</td>
				<td>
					<c:out value="${user.address}"/>
				</td>
				<td>
					<c:out value="${user.telephone}"/>
				</td>
				<td>
					<fmt:formatDate value="${user.loginDate}" pattern="yyyy/MM/dd HH:mm:ss" />
				</td>
				<td>
					<c:if test="${! user.name.equals('***')}">
					<form action="${pageContext.request.contextPath}/admin/delete">
					<input type="hidden" value="${user.id}" name = "id"/>
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">削除確認画面</button>
					<div class="modal fade" id="myModal">
  					<div class="modal-dialog">
    				<div class="modal-content">
      				<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title">削除確認画面</h4>
     				</div>
      				<div class="modal-body">削除確認です。本当に削除しますか？</div>
      				<div class="modal-footer">
      				<input class="btn btn-danger" type="submit" value="削除実行"/>
        			<button type="button" class="btn btn-primary" data-dismiss="modal">やっぱりやめる</button>
       				</div>
    				</div>
  					</div>
					</div>
					</form>
					</c:if>
					<c:if test="${user.name.equals('***')}">
					<span class="text-warning">退会済み</span>
					</c:if>
				</td>
				<td>
					<a href="${pageContext.request.contextPath}/admin/update?id=${user.id}" class="btn btn-primary">変更処理画面へ</a>
				</td>
			  </tr>	
			  </c:forEach>
			</table>
		</div>
	</div>
</div>


  </body>
</html>