<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>

  <body>
    <div class="container">
	<h3>レビュー画面</h3>
	<div class="span8">
		<div class="row">
		<form:form modelAttribute="reviewForm" action="${pageContext.request.contextPath}/user/reviewAction">
			<table class="table table-striped">
			  <tr>
			    <th>
			      	名前
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<form:input path="reviewer" class="form-control" placeholder="名前"/><br><form:errors path="reviewer" cssStyle="color:red"/>
			    </div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			     	 タイトル
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<form:input path="title" class="form-control" placeholder="タイトル"/><br><form:errors path="title" cssStyle="color:red"/>
			    </div>
			    </td>
			  </tr>
			　　<tr>
			    <th>
			      	評価
			    </th>
			    <td>
			    	 <input type=radio name="score" value="1">1
  					 <input type=radio name="score" value="2">2
  					 <input type=radio name="score" value="3" checked>3
  					 <input type=radio name="score" value="4">4
  					 <input type=radio name="score" value="5">5
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	コメント
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<form:textarea path="comment" class="form-control" placeholder="コメント"/><br><form:errors path="comment" cssStyle="color:red"/>
			    </div>
			    </td>
			  </tr>
			  <tr>
			  	<td></td>
			    <td>
			    	<input type="hidden" name="itemId" value="${itemId}" />
					<input class="btn" type="submit" value="登録">
			    </td>
			  </tr>
			</table>
		  </form:form>
		</div>
	</div>
</div>
  </body>
</html>