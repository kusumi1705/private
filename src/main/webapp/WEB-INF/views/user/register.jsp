<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>ユーザ登録画面</h3>
	<div class="span8">
		<div class="row">
		<form:form modelAttribute="registerForm" action="${pageContext.request.contextPath}/user/registerAction">
			<table class="table table-striped">			
			  <tr>
			    <th>
			     	 名前
			    </th>
			    <td>
			     <div class="col-xs-6">
			    	<form:input path="name" class="form-control" placeholder="名前"/><br><form:errors path="name" cssStyle="color:red"/>
			    	</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	メールアドレス
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<form:input path="email" class="form-control" placeholder="メールアドレス"/><br><form:errors path="email" cssStyle="color:red"/>
			    </div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			    	住所
			  　　 </th>
			    <td>
			    	<!-- ▼郵便番号入力フィールド(7桁) -->
					&nbsp;&nbsp;&nbsp;&nbsp;郵便番号&nbsp;&nbsp;&nbsp;&nbsp;例　　140-0015<br>
					<div class="col-xs-3">
					<input class="form-control" type="text" name="zip11"  class="form-control" value="${zip}" placeholder="郵便番号" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','addr11','addr11');"><br>
					</div><br><br><br>
					<!-- ▼住所入力フィールド(都道府県+以降の住所) -->&nbsp;&nbsp;&nbsp;&nbsp; 住所　　郵便番号を入力すれば自動的に出ます<br>
					<div class="col-xs-6">
					<input class="form-control" type="text" name="addr11"  class="form-control" placeholder="住所" size="10" maxlength="8"><br>
					<form:errors path="addr11" cssStyle="color:red"/>
					</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	電話番号
			    </th>
			    <td>
			    	&nbsp;&nbsp;&nbsp;&nbsp;例　　080-6584-7273<br>
			    	<div class="col-xs-6">
			    	<form:input path="telephone" class="form-control" placeholder="電話番号"/><br><form:errors path="telephone" cssStyle="color:red"/>
			    	</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			     	 パスワード
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<form:password path="password" class="form-control" placeholder="パスワード"/><br><form:errors path="password" cssStyle="color:red"/>
			   	</div>
			    </td>
			  </tr>
			  <tr>
			  	<th>
			  		確認用パスワード
			  	</th>
			  	<td>
			  	<div class="col-xs-6">
			  		<form:password path="confirmPassword" class="form-control" placeholder="確認用パスワード"/><br><form:errors path="confirmPassword" cssStyle="color:red"/>
			  	</div>
			  	</td>
			  </tr>
			  <tr>
			  	<td></td>
			    <td>
					<input class="btn" type="submit" value="登録">
			    </td>
			  </tr>
			</table>
		  </form:form>
		</div>
	</div>
</div>


  </body>
</html>