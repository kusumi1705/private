<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>退会処理画面</h3>
			<div class="row">
			<div
				class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
				<h3 class="text-center">ユーザ退会処理が完了しました！</h3>
				<h3 class="text-center">この度は当サイトを使って頂きありがとうございました。</h3>
				<h3 class="text-center">またの機会をお待ちしております。。。</h3>
				<br>
			</div>
		</div>
	
		<div class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
			<div class="text-center">
			
			<br>
			<br>
			<img src="${pageContext.request.contextPath}/image/ロゴたち.png" alt="イラスト２" width=500 height=200>
			<br>
			</div>
	   </div>
	
</div>


  </body>
</html>