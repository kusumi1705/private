<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>退会処理画面</h3>
	<div class="span8">
		<div class="row">
			<div
				class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
				<h3 class="text-center">退会処理画面です！</h3>
				<p class="text-center">本当に退会しますか？</p>
				<p class="text-center">退会する場合は、パスワードをお願いいたします。</p>
				<br>
			</div>
		
			<form:form modelAttribute="updateForm" action="${pageContext.request.contextPath}/user/delete" >
			<table class="table table-striped">			
			  <tr>
			    <th>
			     	 パスワード
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<form:password path="password" class="form-control" placeholder="パスワード"/><br><form:errors path="password" cssStyle="color:red"/>
			   	</div>
			    </td>
			  </tr>
			  <tr>
			  	<th>
			  		確認用パスワード
			  	</th>
			  	<td>
			  	<div class="col-xs-6">
			  		<form:password path="confirmPassword" class="form-control" placeholder="確認用パスワード"/><br><form:errors path="confirmPassword" cssStyle="color:red"/>
			  	</div>
			  	</td>
			  </tr>
			  <tr>
			  	<td></td>
			    <td>
				

			<input type="hidden" value="${userInfo.id}" name = "id"/>
			<input class="btn btn-danger" type="submit" value="退会処理実行"/></td>
			
			  </tr>
			</table>
		  </form:form>
		</div>
	</div>
</div>


  </body>
</html>