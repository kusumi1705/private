<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>ユーザ情報変更完了画面</h3>
			<div class="row">
			<div
				class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
				<h3 class="text-center">ユーザ情報変更が完了しました！</h3>
				<p class="text-center">下記に情報を記載します！ご確認お願いいたします！</p>
				<br>
			</div>
		</div>
	
	<div class="span8">
		<div class="row">
			<table class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12able table-striped">			
			  <tr>
			    <th>
			     	<div class="text-center">名前</div>
			    </th>
			    <td>
			    	<c:out value="${userInfo.name}" />
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	<div class="text-center">メールアドレス</div>
			    </th>
			    <td>
			    		<c:out value="${userInfo.email}" />
			    </td>
			  </tr>
			  <tr>
			    <th>
			      		<div class="text-center">住所</div>
			    </th>
			    <td>
			    		<c:out value="${userInfo.address}"/>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      		<div class="text-center">電話番号</div>
			    </th>
			    <td>
			    		<c:out value="${userInfo.telephone}"/>
			    </td>
			  </tr>
			</table>
		</div>
	</div>
		<div class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
			<div class="text-center">
			
			<span style="font-size:40px" class="text-center"><a href="${pageContext.request.contextPath}/item/list">商品一覧に戻る</a></span>
			<img src="${pageContext.request.contextPath}/image/ho.png" alt="イラスト２" width=70 height=70><br>
			<br>
			<br>
			<img src="${pageContext.request.contextPath}/image/ロゴたち.png" alt="イラスト２" width=500 height=200>
			<br>
			</div>
	   </div>
	
</div>


  </body>
</html>