<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>マイページ画面画面</h3>
	<div class="span8">
		<div class="row">
		
			<table class="table table-striped">			
			  <tr>
			    <th>
			    <div class="text-center">
			     	 <a href="${pageContext.request.contextPath}/user/update">ユーザ情報変更</a>
			    </div>
			    </th>
			  </tr>
			  <tr>
			  	<th>
			  	 <div class="text-center">
			  		<a href="${pageContext.request.contextPath}/order/history">注文履歴</a>	
			  	</div>
			  	</th>
			  </tr>
			  <tr>
			</table>
		</div>
	</div>
</div>


  </body>
</html>