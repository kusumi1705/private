<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>

  <body>
    <div class="container">
	<h3>ユーザ情報変更画面</h3>
	<p>変更したい情報を入力してください</p>
	<div class="span8">
		<div class="row">
		<form:form modelAttribute="updateForm" action="${pageContext.request.contextPath}/user/updateAction">
		<table class="table table-striped">			
			  <tr>
			    <th>
			     	 名前
			    </th>
			    <td>
			     <div class="col-xs-6">
			    	<input type="text" name="name" value="${userInfo.name}" class="form-control" placeholder="名前"/><br><form:errors path="name" cssStyle="color:red"/>
			    	</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	メールアドレス
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<input type="text" name="email" value="${userInfo.email}" class="form-control" placeholder="メールアドレス"/><br><form:errors path="email" cssStyle="color:red"/>
			    </div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			    	住所
			  　　 </th>
			    <td>
			    <div class="col-xs-6">
					<input class="form-control" type="text" name="address" value="${userInfo.address}" placeholder="住所"/><br><form:errors path="address" cssStyle="color:red"/>
				</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	電話番号
			    </th>
			    <td>
			    	&nbsp;&nbsp;&nbsp;&nbsp;例　　080-6584-7273<br>
			    	<div class="col-xs-6">
			    	<input type="text" name="telephone" value="${userInfo.telephone}" class="form-control" placeholder="電話番号"/><br><form:errors path="telephone" cssStyle="color:red"/>
			    	</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			     	 現在のパスワード
			    </th>
			    <td>
			    <div class="col-xs-6">
			    	<form:password path="password" class="form-control" placeholder="現在のパスワード"/><br><form:errors path="password" cssStyle="color:red"/>
			   	</div>
			    </td>
			  </tr>
			  <tr>
			  	<th>
			  		新しいパスワード
			  	</th>
			  	<td>
			  	<div class="col-xs-6">
			  		<form:password path="newPassword" class="form-control" placeholder="新しいパスワード"/><br><form:errors path="confirmPassword" cssStyle="color:red"/>
			  	</div>
			  	</td>
			  </tr>
			  <tr>
			  	<th>
			  		確認用パスワード
			  	</th>
			  	<td>
			  	<div class="col-xs-6">
			  		<form:password path="confirmPassword" class="form-control" placeholder="確認用パスワード"/><br><form:errors path="confirmPassword" cssStyle="color:red"/>
			  	</div>
			  	</td>
			  </tr>
		
		<!--  		
			<table class="table table-striped">
			  <tr>
			    <th>
			     	 名前
			    </th>
			    <td>
			    	<form:input path="name" value="${userInfo.name}" placeholder="名前"/><br><form:errors path="name" cssStyle="color:red"/>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	メールアドレス
			    </th>
			    <td>
			    	<form:input path="email" value="${userInfo.email}" placeholder="メールアドレス"/><br><form:errors path="email" cssStyle="color:red"/>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	住所
			    </th>
			    <td>
			    	<form:input path="address" value="${userInfo.address}" placeholder="住所"/><br><form:errors path="address" cssStyle="color:red"/>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	電話番号
			    </th>
			    <td>
			    	<form:input path="telephone" value="${userInfo.telephone}" placeholder="電話番号"/><br><form:errors path="telephone" cssStyle="color:red"/>
			    </td>
			  </tr>
			  <tr>
			    <th>
			     	 現在のパスワード
			    </th>
			    <td>
			    	<form:password path="password" placeholder="パスワード"/><br><form:errors path="password" cssStyle="color:red"/>
			    </td>
			  </tr>
			  <tr>
			    <th>
			     	 新しいパスワード
			    </th>
			    <td>
			    	<form:password path="newPassword" placeholder="パスワード"/><br><form:errors path="newPassword" cssStyle="color:red"/>
			    </td>
			  </tr>
			  <tr>
			  	<th>
			  		確認用パスワード
			  	</th>
			  	<td>
			  		<form:password path="confirmPassword" placeholder="確認用パスワード"/><br><form:errors path="confirmPassword" cssStyle="color:red"/>
			  	</td>
			  </tr>
			  -->
			  <tr>
			  	<td></td>
			    <td>
			    	<input type="hidden" value="${userInfo.id}" name = "id"/>
					<input class="btn" type="submit" value="登録">
			    </td>
			  </tr>
			</table>
		  </form:form>
		</div>
	</div>
	<br>
	<br>
	<br>
	<table class="table table-striped">
		<tr>
			<td></td><td><a href="${pageContext.request.contextPath}/user/deleteform">退会しますか</a><td>
		</tr>
	</table>
</div>


  </body>
</html>