<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<body>
<script>
$(function() {

var box = document.getElementById("box");
var btn = document.getElementById("btn");
var close = document.getElementById("close");
var boxstyle = box.style;

btn.onclick = function(){
  if( boxstyle.display == "block" ) {
    boxstyle.display = "none";
  } else {
    boxstyle.display = "block";
  }
};

close.onclick = function(){
  boxstyle.display = "none";
};

});
</script>
  
    <div class="container">
        <h3>ログイン画面</h3>
		<div class="row">
		<span style="color:red"><c:out value="${error}"/></span>
		<form:form modelAttribute="loginForm" action="${pageContext.request.contextPath}/user/loginAction">
			<table class="table table-striped">
			  <tr>
			    <th>
			    	 メールアドレス
			    </th>
			    <td>
			   		<div class="col-xs-5">
			    		<form:input class="form-control" path="email" placeholder="Email" />
			    		<form:errors path="email" />
			    	</div>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	パスワード
			    </th>
			    <td>
			    	<div class="col-xs-5">
			    		<form:password class="form-control" path="password" placeholder="Password"/>
			    		<form:errors path="password" />
			    	</div>
			    </td>
			  </tr>
			  <tr>
			  	<td></td>
			    <td>
					<input class="btn" type="submit" value="ログイン">
			    </td>
			  </tr>
			</table>
		  </form:form>
		  <a href="${pageContext.request.contextPath}/user/register" id="toInsertMember">ユーザ登録はこちらから</a><br><br>
		  <input type="button" id="btn" value="テスト用アカウントはこちらを押してください">
    	  <div id="box">
      	  <p id="close">×</p>
      	  <p>■管理者　　　　 メールアドレス:admin@admin　　パスワード：password</p>
		  <p>■一般　　　　　メールアドレス:test@test　　　　パスワード:password</p>
       </div>
	</div>
</div>

</body>
</html>