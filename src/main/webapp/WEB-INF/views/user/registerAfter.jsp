<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ include file = "/common/common.jsp" %>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <body>
    <div class="container">
	<h3>ユーザ登録後画面</h3>
			<div class="row">
			<div
				class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
				<h3 class="text-center">ユーザ登録が完了しました！</h3>
				<p class="text-center">この度はユーザのご登録ありがとうございます。</p>
				<p class="text-center">お買い物を続ける場合は、ログインをお願いいたします。</p>
				<br>
			</div>
		</div>
	
	<div class="span8">
		<div class="row">
			<table class="table table-striped">			
			  <tr>
			    <th>
			     	 名前
			    </th>
			    <td>
			    	<c:out value="${userInfo.name}" />
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	メールアドレス
			    </th>
			    <td>
			    	<c:out value="${userInfo.email}" />
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	住所
			    </th>
			    <td>
			    	<c:out value="${userInfo.address}"/>
			    </td>
			  </tr>
			  <tr>
			    <th>
			      	電話番号
			    </th>
			    <td>
			    	<c:out value="${userInfo.telephone}"/>
			    </td>
			  </tr>
			</table>
		</div>
	</div>
		<div class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
			<div class="text-center">
			
			<span style="font-size:40px" class="text-center"><a href="${pageContext.request.contextPath}/user/login">ログインする</a></span>
			<img src="${pageContext.request.contextPath}/image/doya.png" alt="イラスト２" width=70 height=70><br>
			<br>
			<br>
			<img src="${pageContext.request.contextPath}/image/ロゴたち.png" alt="イラスト２" width=500 height=200>
			<br>
			</div>
	   </div>
	
</div>


  </body>
</html>