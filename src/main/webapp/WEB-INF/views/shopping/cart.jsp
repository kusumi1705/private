<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="all">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.1.js"></script>
<script type="text/javascript">
function changeFunc(eo) {
	var changeNum = $(eo.target).val();
	$(eo.target).next().val(changeNum);
	$(eo.target).parent().submit();
}
$(function () {
	$(this).change(changeFunc);
});

</script>
<body>
<div class="container">
	<h2 class="text-center">ショッピングカート</h2>
	<c:if test="${cartNum == 0 || cartNum == null}">
				<div class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
			<h3 class="text-center">カートに商品はありません</h3>
		</div>
	</c:if>
	<div class="table-responsive col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-10 col-xs-12">
	<c:if test="${itemList != null}">
	
		<!-- ログインしていない人の処理（セッション） -->
		<c:if test="${user == null && cartNum > 0}">
			<c:forEach var="item" items="${itemList}">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th>
								<div class="text-center">
									商品写真
								</div>
							</th>
							<th>
								<div class="text-center">
									商品名
								</div>
							</th>
							<th>
								<div class="text-center">
									数量
								</div>
							</th>
							<th>
								<div class="text-center">
									小計
								</div>
							</th>
							<th>
							</th>
						</tr>
						<tr>
							<td>
								<div class="center">
									<img src="${item.imgCode}" width="200px" height="200px"/>
								</div>
							</td>
							<td>
								<div class="text-center">
									【<c:out value="${item.name}"/>】
								</div>
							</td>
							<td>
								<c:out value="${item.quantity}"/><br>
							</td>
							<td>
								<fmt:formatNumber value="${item.itemTotal}" pattern="###,###"/>yen<br>
							</td>
							<td>
								<form:form modelAttribute="shoppingCartForm" action="${pageContext.request.contextPath}/cart/delete">
									<input type="submit" value="削除">
									<input type="hidden" name="itemId" value="${item.id}">
									<input type="hidden" name="quantity" value="${item.quantity}">
								</form:form>
							</td>
						</tr>
					</tbody>
				</table>

			</c:forEach>
			<div class="row">
				<div class="col-xs-offset-2 col-xs-8">
					<div class="form-group text-center">
			<span id="total-price">合計<fmt:formatNumber value="${total}" pattern="###,###"/>yen(税抜き)</span><br><br>
			<span id="total-price">合計<fmt:formatNumber value="${taxTotal}" pattern="###,###"/>yen（税込）</span>
				</div>
				</div>
			</div>
						<div class="row">
				<div class="col-xs-offset-5 col-xs-3">
					<div class="form-group">

				<form:form modelAttribute="orderForm" action="${pageContext.request.contextPath}/order/ordersCheckList">
					<input type="submit" value="購入する" class="form-control btn btn-warning btn-block">
					<input type="hidden" name="itemList" value="${itemList}">
					<input type="hidden" name="totalPrice" value="${total}">
				</form:form>
									</div>
				</div>
			</div>
			</div>
		</c:if>
	</c:if>
	<c:if test="${fn:length(cartList) != 0}">
		<!-- ログインしている人の処理（DB） -->
		<c:if test="${user != null}">
			<table class="table table-striped">
				<tbody>
					<c:forEach var="cart" items="${cartList}">
						<c:forEach var="items" items="${cart.itemsList}">
							<tr>
								<th>
									<div class="text-center">
										商品画像
									</div>
								</th>
								<th>
									<div class="text-center">
										商品名
									</div>
								</th>
								<th>
									<div class="text-center">
										価格(税抜)
									</div>
								</th>
								<th>
									<div class="text-center">
										数量
									</div>
								</th>
								<th>
									<div class="text-center">
										小計
									</div>
								</th>
							</tr>
							<tr>
							<td>
								<div class="center">
									<img src="${items.imgCode}" width="200px" height="200px"/>
								</div>
							</td>
							<td>
								【<c:out value="${items.name}"/>】<br>
							</td>
							<td>
								<fmt:formatNumber value="${items.price}" pattern="###,###"/>yen<br>
							</td>
						
					</c:forEach>
						<td>
						<form:form modelAttribute="shoppingCartForm" action="${pageContext.request.contextPath}/cart/change">
							<select name="changeQuantity">
								<c:forEach begin="1" end="${cart.quantity + 5}" varStatus="status">
									<c:if test="${status.count == cart.quantity}">
										<option value="${status.count}" selected><c:out value="${status.count}"/></option>
									</c:if>
									<c:if test="${status.count != cart.quantity}">
										<option value="${status.count}" ><c:out value="${status.count}"/></option>
									</c:if>
								</c:forEach>
							</select>
							<input type="hidden" name="hiddenquantity" value="">
							<input type="hidden" name="quantity" value="${cart.quantity }">
							<input type="hidden" name="itemId" value="${cart.itemId }">
							</form:form>
						</td>
						<td>
							<fmt:formatNumber value="${cart.totalPrice}" pattern="###,###"/>yen
							<form:form modelAttribute="shoppingCartForm" action="${pageContext.request.contextPath}/cart/delete">
								<input type="submit" value="削除">
								<input type="hidden" name="id" value="${cart.id}">
							</form:form>
						</td>
					 </tr>
					</c:forEach>
				</tbody>
			</table>
			
			<div class="row">
				<div class="col-xs-offset-2 col-xs-8">
					<div class="form-group text-center">
						<span id="total-price">合計<fmt:formatNumber value="${totalPrice}" pattern="###,###"/>yen(税抜き)</span><br>
						<span id="total-price">合計<fmt:formatNumber value="${taxTotal}" pattern="###,###"/>yen（税込）</span>
					</div>
				</div>
			</div>
			<span id="calcOutput"></span>
			<div class="row">
				<div class="col-xs-offset-5 col-xs-3">
					<div class="form-group">
						<form:form modelAttribute="orderForm" action="${pageContext.request.contextPath}/order/ordersCheckList">
							<input type="submit" value="購入する" class="form-control btn btn-warning btn-block">
							<input type="hidden" name="itemList" value="${itemList}">
						</form:form>
					</div>
				</div>
			</div>
		</div>
		</c:if>
	</c:if>
	</div>
	</div>
	<div class="bottom1"></div>
</body>
</html>