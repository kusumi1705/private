package jp.co.rakus.ecommers.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 商品クラス
 * 
 * @author 鳥屋部
 *
 */
public class Items {
	private Integer id; 
	//商品の名前
	private String name;
	//商品コード
	private String productCode;
	//商品の値段
	private Integer price;
	//商品説明
	private String description;
	//商品画像データ
	private String imgCode;
	//商品の合計金額
	private Integer itemTotal;
	//評価の平均
	private double scoreAverage;
	//数量
	private Integer quantity;
	//商品のレビュー情報
	private List<Review> reviewList;
	//評価の表示
	private String scoreSymbol;

	
	/**
	 * 商品の合計金額を表示
	 * 
	 * @param quantity
	 */
	public void calcItemTotal(Integer quantity){
		this.itemTotal = quantity * price;
	}
	
	
	/**
	 * reviewListにReviewオブジェクトを追加
	 * 
	 * @param review
	 */
	public void add(Review review){
		this.reviewList.add(review);
		calcScoreAverage(review.getScore());
		//this.scoreAverage = (this.scoreAverage + review.getScore()) / 2;
	}
	
	/**
	 * スコアの平均を計算
	 * 
	 * @param score
	 */
	private void calcScoreAverage(int score){
		if(this.scoreAverage == 0.0){
			this.scoreAverage = score;
		}else{
			this.scoreAverage = (this.scoreAverage + score )/2 ;
		}
		this.scoreSymbol = addScoreSymbol(this.scoreAverage);
	}
	
	/**
	 * スコアの記号表示を作成
	 * 
	 * @param score
	 * @return
	 */
	private String addScoreSymbol(double score){
		String symbolTrue = "★";
		String symbolFalse = "☆";
		String scoreSymbol = "";
		for(int i = 1; i <= 5; i++){
			if(i <= score){
				scoreSymbol += symbolTrue;
			}else{
				scoreSymbol += symbolFalse;
			}
		}
		return scoreSymbol;
	}
	




}
