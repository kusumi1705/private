package jp.co.rakus.ecommers.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItems {

	private Integer id;
	private Integer orderId;
	private Integer quantity;
	private Integer totalPrice;
	private List<Items> itemsList;

}
