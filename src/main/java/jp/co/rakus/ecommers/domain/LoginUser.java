package jp.co.rakus.ecommers.domain;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
	public class LoginUser extends org.springframework.security.core.userdetails.User {

		/**
		 * シリアルバージョンUID.
		 */
		private static final long serialVersionUID = 1L;
		private User user;
	 
		/**
		 * 通常のユーザー情報に加え、認可用ロールを設定する.
		 * 
		 * @param user
		 * ユーザー情報
		 */		
		public LoginUser(User user, List<GrantedAuthority> createAuthorityList) {
			super(user.getEmail(), user.getPassword(), createAuthorityList);
			this.user = user;
		}
	}
