package jp.co.rakus.ecommers.domain;

import java.sql.Timestamp;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
	private Integer id;
	private String name;
	private String email;
	private String address;
	private String telephone;
	private String password;
	private Timestamp loginDate;
	private Integer role;
}
