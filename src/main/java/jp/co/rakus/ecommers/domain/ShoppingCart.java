package jp.co.rakus.ecommers.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingCart {
	private Integer id;
	private Integer userId;
	private Integer itemId;
	private Integer orderId;
	private Integer quantity;
	private Integer totalPrice;
	private List<Items> itemsList;
	/**
	 * 
	 * @param items
	 */
	public void add(Items items){
		itemsList.add(items);
	}
}
