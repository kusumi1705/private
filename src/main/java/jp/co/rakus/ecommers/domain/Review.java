package jp.co.rakus.ecommers.domain;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Review {

	private Integer id;
	// ユーザID
	private Integer userId;
	// レビュータイトル
	private String title;
	//　レビューする人の名前
	private String reviewer;
	// 商品ID
	private Integer itemId;
	// レビュー投稿日時
	private Date postDate;
	//　レビュー投稿内容
	private String comment;
	// 商品評価　1 ~ 5
	private Integer score;
	//評価の表示
	private String scoreSymbol = "";
	
	private String symbolTrue = "★";
	private String symbolFalse = "☆";

	public Review(Integer id, Integer userId, String title, String reviewer, Integer itemId, Date postDate,
			String comment, Integer score, String scoreSymbol) {
		this.id = id;
		this.userId = userId;
		this.title = title;
		this.reviewer = reviewer;
		this.itemId = itemId;
		this.postDate = postDate;
		this.comment = comment;
		this.score = score;
		addScoreSymbol(score);
	}
	
	private void addScoreSymbol(int score){
		for(int i = 1; i <= 5; i++){
			if(i <= score){
				this.scoreSymbol += symbolTrue;
			}else{
				this.scoreSymbol += symbolFalse;
			}
		}
	}

}
