package jp.co.rakus.ecommers.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orders {

	private Integer id;
	private Integer userId;
	private Integer status;
	private Integer totalPrice;
	private Date orderDate;
	private String destinationName;
	private String destinationEmail;
	private String destinationAddress;
	private String destinationTel;
	private Date deliveryTime;
	private Integer paymentMethod;
	private Integer orderItemsId;
	private User user;
	private List<Items> orderItemsList;

	public void add(Items item) {
		this.orderItemsList.add(item);
	}

}
