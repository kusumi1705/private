package jp.co.rakus.ecommers.domain;

import java.util.Comparator;

public class ComparatorRank implements Comparator<Rank> {
	
	@Override 
	public int compare(Rank p1, Rank p2) {
		return p1.getNum() > p2.getNum() ? -1 : 1; 
	}

}
