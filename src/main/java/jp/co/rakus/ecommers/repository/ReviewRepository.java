package jp.co.rakus.ecommers.repository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.Review;

@Repository
public class ReviewRepository {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	/**
	 * ResultSetオブジェクトからUserオブジェクトに変換するためのクラス実装&インスタンス化
	 */
	private static final RowMapper<Review> reviewRowMapper = (rs, i) -> {
		Integer id = rs.getInt("id");
		Integer userId = rs.getInt("user_id");
		String title = rs.getString("title");
		String reviewer = rs.getString("reviewer");
		Integer itemId = rs.getInt("item_id");
		Date postDate = rs.getDate("post_date");
		String comment = rs.getString("comment");
		Integer score = rs.getInt("score");
		return new Review(id, userId, title, reviewer, itemId, postDate, comment, score, null);
	};
	
	/**
	 * レビュー情報を登録する
	 * 
	 * @param review
	 */
	public void save(Review review) {

		SqlParameterSource param = new BeanPropertySqlParameterSource(review);
		//if (review.getId() == null) {
			jdbcTemplate.update(
					"INSERT INTO review(user_id, title, reviewer, item_id, comment, score) values(:userId, :title, :reviewer, :itemId, :comment, :score)", 
					param);
		//} else {
		//	jdbcTemplate.update(
		//			"UPDATE members SET name=:name,email=:email,password=:password WHERE id=:id", 
		//			param);
		//}
		//return user;
	}
	
	public List<Review> findAll(){
		String sql = "SELECT id, user_id, title, reviewer, item_id, post_date, comment, score "
				+ "     FROM review ORDER BY id";
		
		List<Review> reviewList = jdbcTemplate.query(sql, reviewRowMapper);
		return reviewList;
	}
	
	/**
	 *　idに紐づいたレビュー情報を取得
	 * 
	 * @param id
	 * @return
	 */
	public List<Review> loadOne(Integer id){
		String sql = "SELECT id, user_id, title, reviewer, item_id, post_date, comment, score "
				+ "     FROM review WHERE item_id = :id";
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("id", id);
		List<Review> review = jdbcTemplate.query(sql, param, reviewRowMapper);
		return review;
	}
}
