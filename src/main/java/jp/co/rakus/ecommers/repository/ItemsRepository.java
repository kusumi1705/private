package jp.co.rakus.ecommers.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.Review;

@Repository
public class ItemsRepository {

	@Autowired
	private NamedParameterJdbcTemplate template;
	
	private static final RowMapper<Items> itemsRowMapper = (rs, i) ->{
		Integer id = rs.getInt("id"); 
		String name = rs.getString("name");
		String productCode = rs.getString("product_code");
		Integer price = rs.getInt("price");
		String description = rs.getString("description");
		String imgCode = rs.getString("img_code");
		return new Items(id, name, productCode, price, description,imgCode, null, 0.0, null, new ArrayList<Review>(), null); 

	};
	
	public List<Items> findAll(){
		String sql = "SELECT id, name, product_code, price, description, img_code "
				+ "     FROM items where deleted = false ORDER BY id";
		
		List<Items> itemList = template.query(sql, itemsRowMapper);
		return itemList;
	}
	
	public List<Items> findSearch(String name){
		String sql = "SELECT id, name, product_code, price, description, img_code "
				+ "     FROM items WHERE UPPER(name) LIKE UPPER(:name) ORDER BY id ";
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("name", "%" + name + "%");
		List<Items> itemsList = template.query(sql, param, itemsRowMapper);
		return itemsList;
	}
	
	public List<String> findSearchName(String name){
		String sql = "SELECT name FROM items WHERE UPPER(name) like UPPER(:name)";
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("name", name + "%");
		List<String> itemsList = template.queryForList(sql, param, String.class);
		return itemsList;
	}
	
	public Items loadOne(Integer id){
		String sql = "SELECT id, name, product_code, price, description, img_code "
				+ "     FROM items WHERE id = :id";
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("id", id);
		Items item = template.queryForObject(sql, param, itemsRowMapper);
		return item;
	}
	
	
	/**
	 * idに紐づく商品情報を変更
	 * 
	 * @param item
	 */
	public void updateItem(Items item){
		SqlParameterSource param = new BeanPropertySqlParameterSource(item);
		
		//画像データが入っていない場合、画像データは更新しない
		if(item.getImgCode().equals("")){
			String sql = "UPDATE items SET "
					+ "name = :name, price = :price, description = :description WHERE id = :id";
			template.update(sql, param);
		}else{
		
			String sql = "UPDATE items SET "
				+ "name = :name, price = :price, description = :description, img_code = :imgCode WHERE id = :id";
			template.update(sql, param);
		}
	}
		
	
	/**
	 * idに紐づく商品情報を削除（deletedフラグtrueに変更）
	 * 
	 * @param id
	 */
	public void deleteItem(Integer id){
		String sql = "UPDATE items SET deleted = true WHERE id = :id";
		SqlParameterSource param = new MapSqlParameterSource().addValue("id", id);
		template.update(sql, param);
	}
	
	/**
	 * 全ての商品情報を削除（deletedフラグtrueに変更）
	 * 
	 */
	public void deleteAllItem(){
		String sql = "UPDATE items SET deleted = true";
		SqlParameterSource param = new MapSqlParameterSource();
		template.update(sql, param);
	}
	
	/**
	 * 商品情報を追加
	 * 
	 * @param item
	 */
	public void insertItem(Items item){
		SqlParameterSource param = new BeanPropertySqlParameterSource(item);
		String sql = "INSERT INTO items(name, product_code, price, description, category_id, img_code) values(:name, 'test', :price,:description, 8, :imgCode)";
		template.update(sql, param);
	}
	
	/**
	 * 商品情報を更新する
	 * 
	 * @param item
	 */
	public void updateAllItem(Items item){
		SqlParameterSource param = new BeanPropertySqlParameterSource(item);
		String sql = "UPDATE items SET name = :name, price = :price, description = :description, img_code = :imgCode";
		template.update(sql, param);
	}
	
	/**
	 * ページャに紐づく商品を取得する
	 * 
	 * @param pageNum
	 * @return
	 */
	public List<Items> findByPage(Integer pageNum, Integer displayNum){
		SqlParameterSource param = new MapSqlParameterSource().addValue("pageNum", pageNum).addValue("displayNum", displayNum);
		String sql = "SELECT id, name, product_code, price, description, img_code "
				+ "     FROM items where deleted = false ORDER BY id limit :displayNum offset :pageNum ";
		
		List<Items> itemList = template.query(sql, param, itemsRowMapper);
		return itemList;
	}
	
	/**
	 * 指定された表示数の商品情報を取得
	 * 
	 * @param displayNum 表示数
	 * @return
	 */
	public List<Items> loadByDisplayNum(Integer displayNum){
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("displayNum", displayNum);
		String sql = "SELECT id, name, product_code, price, description, img_code "
				+ "     FROM items where deleted = false ORDER BY id limit :displayNum offset 0 ";
		List<Items> itemList = template.query(sql, param, itemsRowMapper);
		
		return itemList;
	}
	
}
