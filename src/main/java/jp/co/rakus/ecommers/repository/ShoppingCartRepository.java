package jp.co.rakus.ecommers.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.ShoppingCart;

@Repository
public class ShoppingCartRepository {

	@Autowired
	NamedParameterJdbcTemplate template;

	public static final RowMapper<ShoppingCart> shoppingCartRowMapper = (rs, i) -> {
		Integer id = rs.getInt("id");
		Integer userId = rs.getInt("user_id");
		Integer itemId = rs.getInt("item_id");
		Integer orderId = rs.getInt("order_id");
		Integer quantity = rs.getInt("quantity");
		Integer totalPrice = rs.getInt("total_price");
		
		return new ShoppingCart(id, userId, itemId, orderId, quantity, totalPrice, new ArrayList<Items>());

	};

	/**
	 * カートに新しい商品が入れられる毎に情報を追加しています
	 * 
	 * @param itemId
	 * @param orderId
	 * @param quantity
	 * @param itemTotal
	 */

	public void upDateOrderItems(Integer itemId, Integer userId, Integer quantity, Integer itemTotal) {
		String sql = "INSERT INTO order_items(item_id, user_id, quantity, total_price, buy_status) "
				+ "   VALUES (:itemId, :userId, :quantity, :itemTotal, 0)";
		SqlParameterSource param = new MapSqlParameterSource().addValue("itemId", itemId).addValue("userId", userId)
				.addValue("quantity", quantity).addValue("itemTotal", itemTotal);
		template.update(sql, param);

	}

	/**
	 * 再ログインした人のカートの中身を再表示する
	 * 
	 * @param userId
	 * @return
	 */
	public List<ShoppingCart> findOrderItems(Integer userId) {
		String sql = "SELECT id, item_id, user_id, order_id, quantity, total_price"
				+ "     FROM order_items WHERE user_id = :userId and buy_status = 0 ORDER BY id";

		SqlParameterSource param = new MapSqlParameterSource().addValue("userId", userId);
		List<ShoppingCart> cartList = template.query(sql, param, shoppingCartRowMapper);
		return cartList;
	}

	/**
	 * カートの削除ボタンが押されたら商品情報を削除します
	 * 
	 * @param id
	 */
	public void delete(Integer id) {

		String sql = "DELETE FROM order_items WHERE id = :id";

		SqlParameterSource param = new MapSqlParameterSource().addValue("id", id);
		template.update(sql, param);
	}

	/**
	 * 
	 * @param itemId
	 * @return
	 */
	public ShoppingCart find(Integer itemId, Integer userId) {
		try {
			String sql = "SELECT id,  user_id, item_id, order_id, quantity, total_price "
					+ "     FROM order_items WHERE item_id = :itemId and user_id = :userId and buy_status = 0";

			SqlParameterSource param = new MapSqlParameterSource().addValue("itemId", itemId).addValue("userId", userId);
			ShoppingCart checkCart = template.queryForObject(sql, param, shoppingCartRowMapper);
			return checkCart;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * カートに既に入っている商品をユーザーが再度カートに追加したとき数量だけ変更します
	 * @param quantity
	 * @param itemId
	 * @param totalPrice
	 */
	public void plus(Integer quantity, Integer itemId, Integer totalPrice, Integer userId) {
		String sql = "UPDATE order_items SET quantity = quantity + :quantity, "
				+ "    total_price = :totalPrice WHERE item_id = :itemId and user_id = :userId and buy_status = 0";
		SqlParameterSource param = new MapSqlParameterSource().addValue("quantity", quantity)
				.addValue("totalPrice", totalPrice).addValue("itemId", itemId).addValue("userId", userId);
		template.update(sql, param);
	}

	/**
	 * BuyStatusを更新します。 片桐 追加
	 * 
	 * @param status
	 * @param orderId
	 */
	public void updateBuyStatus(Integer buyStatus, Integer id) {
		String sql = "UPDATE order_items SET buy_status = :buyStatus WHERE id = :id";
		SqlParameterSource param = new MapSqlParameterSource().addValue("buyStatus", buyStatus).addValue("id", id);
		template.update(sql, param);
	}

	/**
	 * order_idを更新します。 片桐 追加
	 * 
	 * @param orderId
	 * @param id
	 */
	public void updateOrderId(Integer orderId, Integer id) {
		String sql = "UPDATE order_items SET order_id = :orderId WHERE id = :id";
		SqlParameterSource param = new MapSqlParameterSource().addValue("orderId", orderId).addValue("id", id);
		template.update(sql, param);
	}
	
	
	/**
	 * 修正必要
	 * @param itemId
	 * @return
	 */
	public ShoppingCart findByOrderId(Integer orderId) {
		//try {
			String sql = "SELECT id, user_id, item_id, order_id, quantity, total_price "
					+ "     FROM order_items WHERE order_id = :orderId";

			SqlParameterSource param = new MapSqlParameterSource().addValue("orderId", orderId);
			ShoppingCart checkCart = template.queryForObject(sql, param, shoppingCartRowMapper);
			return checkCart;
		//} catch (Exception e) {
		//	return null;
		//}
	}
	
	/**
	 * userの購入済みの情報を取得
	 * 
	 * @param orderId
	 * @return
	 */
	public List<ShoppingCart> findByOrderIdAndStatusBuyed(Integer orderId){
		try {
			String sql = "SELECT id, item_id, order_id, quantity, total_price "
					+ "     FROM order_items WHERE order_id = :orderId AND buy_status = 1";

			SqlParameterSource param = new MapSqlParameterSource().addValue("orderId", orderId);
			List<ShoppingCart> cartList = template.query(sql, param, shoppingCartRowMapper);
			return cartList;
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<ShoppingCart> findAllBuyedStatus() {
		String sql = "SELECT id, item_id, user_id, order_id, quantity, total_price"
				+ "     FROM order_items WHERE buy_status = 1";

		List<ShoppingCart> cartList = template.query(sql, shoppingCartRowMapper);
		return cartList;
	}
}
