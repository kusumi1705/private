package jp.co.rakus.ecommers.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.Orders;

/**
 * ショッピングカートから注文一覧を取得
 * 
 * @author 片桐
 *
 */
@Repository
public class OrdersRepository {

	private static final RowMapper<Orders> ORDERS_ROW_MAPPER = (rs, i) -> {
		Integer id = rs.getInt("id");
		Integer userId = rs.getInt("user_id");
		Integer status = rs.getInt("status");
		Integer totalPrice = rs.getInt("total_price");
		Date orderDate = rs.getDate("order_date");
		String destinationName = rs.getString("destination_name");
		String destinationEmail = rs.getString("destination_email");
		String destinationAddress = rs.getString("destination_address");
		String destinationTel = rs.getString("destination_tel");
		Date deliveryTime = rs.getDate("delivery_time");
		Integer paymentMethod = rs.getInt("payment_method");
		Integer orderItemsId = rs.getInt("order_items_id");
		return new Orders(id, userId, status, totalPrice, orderDate, destinationName, destinationEmail, destinationAddress, destinationTel, deliveryTime, paymentMethod, orderItemsId, null,
				new ArrayList<Items>());
	};

	@Autowired
	private NamedParameterJdbcTemplate template;

	/**
	 * 注文確認画面に表示する一覧を取得
	 * 
	 * @return
	 */
	public List<Orders> ordersHistoryList(Integer id) {

		List<Orders> ordersCheckList = template.query(
				"SELECT id,user_id,status,total_price,order_date,destination_name,destination_email,"
						+ " destination_address,destination_tel,delivery_time,payment_method FROM orders order by id",
				ORDERS_ROW_MAPPER);
		return ordersCheckList;
	}

	/**
	 * ログインした人がショッピングカートに商品を追加したときordersテーブルに追加
	 * 
	 * @param userId
	 */
	public void upDate(Integer userId) {
		String sql = "INSERT INTO orders (user_id, status, order_items_id) " + "   VALUES (:userId, 0, :orderItemId)";

		SqlParameterSource param = new MapSqlParameterSource().addValue("userId", userId).addValue("orderItemId",
				userId);
		template.update(sql, param);
	}

	/**
	 * 注文履歴の一覧を取得
	 * 
	 * @param userId
	 * @return
	 */
	public List<Orders> findHistory(Integer userId) {
		String sql = " SELECT id, user_id, status,total_price, order_date, destination_name, destination_email, "
				+ " destination_address, destination_tel, delivery_time, payment_method, order_items_id "
				+ " FROM orders where user_id = :userId order by id";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userId", userId);
		List<Orders> ordersList = template.query(sql, param, ORDERS_ROW_MAPPER);
		return ordersList;
	}
	
	/**
	 * 注文履歴の一覧を取得
	 * 
	 * @param userId
	 * @return
	 */
	public List<Orders> findAllHistory() {
		String sql = " SELECT id, user_id, status,total_price, order_date, destination_name, destination_email, "
				+ " destination_address, destination_tel, delivery_time, payment_method, order_items_id "
				+ " FROM orders order by id";
		List<Orders> ordersList = template.query(sql, ORDERS_ROW_MAPPER);
		return ordersList;
	}

	/**
	 * 注文確定ボタンを押下してordersに１件データを追加
	 * 
	 * @param userId
	 * @return
	 */
	public void addOrders(Orders order) {
		SqlParameterSource param = new BeanPropertySqlParameterSource(order);
		String sql = "INSERT INTO orders (user_id, status, total_price, destination_name, destination_email, "
				+ " destination_address, destination_tel, delivery_time, payment_method) "
				+ " values(:userId, :status, :totalPrice, :destinationName, :destinationEmail, :destinationAddress,"
				+ " :destinationTel, :deliveryTime, :paymentMethod) ";
		template.update(sql, param);
	}

	/**
	 * ordersのidを取得
	 * 
	 * @return
	 */
	public Integer idLoad() {
		String sql = "SELECT MAX(id) FROM orders";
		SqlParameterSource param = null;
		Integer id = template.queryForObject(sql, param, Integer.class);
		return id;
	}
}
