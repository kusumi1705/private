package jp.co.rakus.ecommers.repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Repository;

import jp.co.rakus.ecommers.domain.Orders;
import jp.co.rakus.ecommers.domain.User;

@Repository
public class UserRepository {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	/**
	 * ResultSetオブジェクトからUserオブジェクトに変換するためのクラス実装&インスタンス化
	 */
	private static final RowMapper<User> userRowMapper = (rs, i) -> {
		Integer id = rs.getInt("id");
		String name = rs.getString("name");
		String email = rs.getString("email");
		String address = rs.getString("address");
		String telephone = rs.getString("telephone");
		String password = rs.getString("password");
		Timestamp loginDate = rs.getTimestamp("login_date");
		Integer role = rs.getInt("role");
		return new User(id, name, email,address,telephone,password,loginDate, role);
	};
	
	public User load(Integer id) {
		SqlParameterSource param = new MapSqlParameterSource()
				.addValue("id",id);
		User user = jdbcTemplate.queryForObject(
				"SELECT id,name,email,password, address,telephone, login_date, role from users where id = :id", param,
				userRowMapper);
		return user;
	}
	
	public User findUserByMailAddress(String email) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email);
		User user = jdbcTemplate.queryForObject(
				"SELECT id,name,email,password, address,telephone, login_date, role from users where email = :email", param,
				userRowMapper);
		return user;
	}
	
	/**
	 * メールアドレスでユーザ情報の数を取得
	 * @param mailAddress　取得したいメンバー情報のアドレス
	 * @return　メンバー情報の数
	 */
	public Integer findByMailAddress(String email){
		String sql = "SELECT count(*) FROM users WHERE email= :email ";
		SqlParameterSource param = new MapSqlParameterSource("email", email);
		Integer num = jdbcTemplate.queryForObject(sql, param, Integer.class);
		
		return num;
	}
	
	
	/**
	 * アドレスからパスワードを取得
	 * 
	 * @param mailAddress
	 * @return
	 */
	public String findByMailAddressGetPassword(String email){
		try{
			String sql = "SELECT password FROM users WHERE email= :email ";
			SqlParameterSource param = new MapSqlParameterSource("email", email);
			String password = jdbcTemplate.queryForObject(sql, param, String.class);
		
			return password;
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	/**
	 * メンバー情報を保存　または　更新する.
	 * 5-3 暗号化処理を追加 
	 * 
	 * @param member　保存または更新するメンバー情報
	 * @return　保存または更新されたメンバー情報
	 */
	public User save(User user) {
		//暗号化処理
		StandardPasswordEncoder passwordEncoder = new StandardPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		SqlParameterSource param = new BeanPropertySqlParameterSource(user);
		if (user.getId() == null) {
			jdbcTemplate.update(
					"INSERT INTO users(name, email, password, address, telephone) values(:name,:email,:password,:address,:telephone)", 
					param);
		} else {
			jdbcTemplate.update(
					"UPDATE members SET name=:name,email=:email,password=:password WHERE id=:id", 
					param);
		}
		return user;
	}
	
	/**
	 * 
	 * メールアドレスからユーザ情報を取得.
	 * @param mailAddress メールアドレス
	 * @return メンバー情報.メンバーが存在しない場合はnull.
	 */
	public User findByMailAddressGetUser(String email) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email);
		User user = null;
		//try{
			user =  jdbcTemplate.queryForObject(
					"SELECT id, name, email, password, address, telephone, login_date, role FROM users WHERE email = :email",
					param,
					userRowMapper);
			return user;
	//	} catch(DataAccessException e) {
	     //	return null;
		//}
	}
	
	/**
	 * ユーザ情報更新
	 *
	 * @param user
	 */
	public void update(User user){
		//暗号化処理
		StandardPasswordEncoder passwordEncoder = new StandardPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		SqlParameterSource param = new BeanPropertySqlParameterSource(user);
		jdbcTemplate.update(
				" UPDATE users SET "
			+   " name = :name,  email = :email, address = :address, password = :password, "
			+   " telephone = :telephone "
			+   " where id = :id ",
				param);
	}

	/**
	 * idに紐づくユーザの情報を削除
	 * 
	 * @param id　
	 */
	public void delete(Integer id){
		String sql = "Update Users SET name = '***', email = :id, address = '***', telephone = '***', deleted = 'true' WHERE id = :id";
		SqlParameterSource param = new MapSqlParameterSource("id", id);
		jdbcTemplate.update(sql, param);
	}
	
	/**
	 * 最終ログインの時間を変更
	 * 
	 * @param id
	 */
	public void updateLoginDate(Integer id){
		String sql = "Update Users SET login_date = now() where id = :id";
		SqlParameterSource param = new MapSqlParameterSource("id", id);
		jdbcTemplate.update(sql, param);
	}
	
	public List<User> findAll(){ 
		List<User> userList = jdbcTemplate.query(
		"SELECT id, name, email, password, address, telephone, login_date, role FROM users order by id",
					userRowMapper);
		return userList;
	}
}
