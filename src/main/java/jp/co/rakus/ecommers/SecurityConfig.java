package jp.co.rakus.ecommers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import jp.co.rakus.ecommers.service.LoginUserDetailsService;

@Configuration
@EnableWebMvcSecurity   // Spring Securityの基本設定
public class SecurityConfig extends WebSecurityConfigurerAdapter {

        @Autowired
        private MyLogoutHandler myLogoutHandler;
	
	    @Override
	    public void configure(WebSecurity web) throws Exception {
	        // セキュリティ設定を無視するリクエスト設定
	        // 静的リソース(images、css、javascript)に対するアクセスはセキュリティ設定を無視する
	        web.ignoring().antMatchers(
	                            "/image/**",
	                            "/css/**",
	                            "/javascript/**",
	                            "/webjars/**",
	                            "/common/**",
	                            "/bootstrap-3.3.7-dist/**",
	                            "/js/**");
	    }

	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        // 認可の設定
	        http.authorizeRequests()
	            .antMatchers("/user/login", "/cart/list", "order/ordersCheckList",
	            		"/user/loginAction","/user/login-failer", "/user/register",
	            		"/user/registerAfter", "/user/registerAction", "/cart/delete",
	            		"/cart/addList", "/item/**", "/").permitAll()
	            .antMatchers("/admin/**").hasRole("ADMIN").anyRequest().authenticated();  

	        // ログイン設定
	        http.formLogin()
	            .loginProcessingUrl("/user/loginAction")   // 認証処理のパス
	            .loginPage("/user/login")            // ログインフォームのパス
	            .failureUrl("/user/login-failer")       // 認証失敗時に呼ばれるハンドラクラス
	            .defaultSuccessUrl("/user/loginSuccess")     // 認証成功時の遷移先
	            .usernameParameter("email").passwordParameter("password")  // アドレス、パスワードのパラメータ名
	            .and();

	        // ログアウト設定
	        http.logout()
	            .logoutRequestMatcher(new AntPathRequestMatcher("/user/logout/sessionInvalidate"))       // ログアウト処理のパス
	            .logoutSuccessUrl("/item/list")
	            .addLogoutHandler(myLogoutHandler);
	        // ログアウト完了時のパス

	    }

	    @Configuration
	    protected static class AuthenticationConfiguration
	    extends GlobalAuthenticationConfigurerAdapter {
	        @Autowired
	        LoginUserDetailsService loginUserDetailsService;

	        
	        @Override
	        public void init(AuthenticationManagerBuilder auth) throws Exception {
	            // 認証するユーザーを設定する
	            auth.userDetailsService(loginUserDetailsService)
	            // 入力値をbcryptでハッシュ化した値でパスワード認証を行う
	            .passwordEncoder(new StandardPasswordEncoder());
	        }
	    
	
	}
}
