package jp.co.rakus.ecommers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import jp.co.rakus.ecommers.domain.User;
import jp.co.rakus.ecommers.service.UserService;

@Component
public class MyLogoutHandler implements LogoutHandler{
	@Autowired
	private UserService userService;
	
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication){
    	User user = (User) request.getSession().getAttribute("user");
    	userService.updateLoginDate(user.getId());
    }
}