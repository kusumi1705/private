package jp.co.rakus.ecommers.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import jp.co.rakus.ecommers.domain.Orders;
import jp.co.rakus.ecommers.domain.User;

@Service
public class AdminService {
	
	/**
	 * 全ユーザー情報をCSVファイルに出力
	 * 
	 * @param userList
	 * @param response
	 */
	public void DownloadUserCsv(List<User> userList, HttpServletResponse response){
		response.setContentType(MimeTypeUtils.APPLICATION_OCTET_STREAM_VALUE + ";charset=Shift_JIS");
        response.setHeader("Content-Disposition", "attachment; filename=\"userAllData.csv\"");
		
		  try {
			    PrintWriter pw = response.getWriter();

	            pw.print("ユーザID,名前,メールアドレス,住所,電話番号");
	            pw.println();
	            
	            for(User user : userList){
	            //内容を指定する
	            	pw.print(user.getId() + "," + user.getName() + ","
	            	+ user.getEmail() + "," + user.getAddress() + "," + user.getTelephone());
	            	pw.println();
	            }

	            //ファイルに書き出す
	            pw.close();

	        } catch (IOException ex) {
	            //例外時処理
	            ex.printStackTrace();
	        }
	}
	
	public void DownloadOrdersCsv(List<Orders> ordersList, HttpServletResponse response){
		response.setContentType(MimeTypeUtils.APPLICATION_OCTET_STREAM_VALUE + ";charset=Shift_JIS");
        response.setHeader("Content-Disposition", "attachment; filename=\"orderAllData.csv\"");
		
		  try {
			    PrintWriter pw = response.getWriter();

	            pw.print("ID,ユーザID,注文状況,合計金額,注文日,届け先名,届け先メールアドレス,届け先住所,届け先電話番号"
	            		+ ",配達予定時間,支払い方法,注文ID");
	            pw.println();
	            
	            for(Orders order : ordersList){
	            //内容を指定する
	            	pw.print(order.getId() + "," + order.getUserId() + ","
	            	+ order.getStatus() + "," + order.getOrderDate() + "," + order.getDestinationName() + ","
	            	+ order.getDestinationEmail() + "," + order.getDestinationAddress() + "," + order.getDestinationTel()
	            	+ order.getDeliveryTime() + "," + order.getPaymentMethod() + "," + order.getOrderItemsId());
	            	pw.println();
	            }
	            
	            //ファイルに書き出す
	            pw.close();

	        } catch (IOException ex) {
	            //例外時処理
	            ex.printStackTrace();
	        }
	}

}
