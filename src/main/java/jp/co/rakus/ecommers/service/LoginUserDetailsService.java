package jp.co.rakus.ecommers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import jp.co.rakus.ecommers.domain.LoginUser;
import jp.co.rakus.ecommers.domain.User;
import jp.co.rakus.ecommers.repository.UserRepository;

@Service
public class LoginUserDetailsService implements UserDetailsService 
{
	@Autowired
	UserRepository userRepository;
	
	private final Integer ADMIN_ROLE = 1;
	
	@Override
	public UserDetails loadUserByUsername(String email) {
		User user = userRepository.findByMailAddressGetUser(email);
		if(user == null){
			throw new UsernameNotFoundException("The requesed user is not found");
		}else if(user.getRole() == ADMIN_ROLE){
			return new LoginUser(user, AuthorityUtils.createAuthorityList("ROLE_ADMIN"));
		}else{
			return new LoginUser(user, AuthorityUtils.createAuthorityList("ROLE_MEMBER"));
		}
	}
}
