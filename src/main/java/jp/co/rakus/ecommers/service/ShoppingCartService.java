package jp.co.rakus.ecommers.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.ShoppingCart;
import jp.co.rakus.ecommers.repository.ItemsRepository;
import jp.co.rakus.ecommers.repository.OrdersRepository;
import jp.co.rakus.ecommers.repository.ShoppingCartRepository;

@Service
public class ShoppingCartService {

	@Autowired
	private ShoppingCartRepository shoppingCartRepository;

	@Autowired
	private ItemsRepository itemsRepository;

	@Autowired
	private OrdersRepository ordersRepository;

	/**
	 * カートに商品が追加されとき商品情報を取ってきます
	 * 
	 * @param itemId
	 * @return
	 */
	public Items loadOne(Integer itemId) {
		return itemsRepository.loadOne(itemId);
	}

	/**
	 * 
	 * @param userId
	 */
	public void updateOrders(Integer userId) {
		ordersRepository.upDate(userId);
	}

	/**
	 * カートに商品が入れられる毎に情報を追加しています
	 * 
	 * @param itemId
	 * @param orderId
	 * @param quantity
	 * @param itemTotal
	 */
	public void upDateOrderItems(Integer itemId, Integer userId, Integer quantity, Integer itemTotal) {
		shoppingCartRepository.upDateOrderItems(itemId, userId, quantity, itemTotal);
	}

	/**
	 * 再ログインした人のカートの中身を再表示する
	 * 
	 * @param userId
	 * @return
	 */
	public List<ShoppingCart> findOrderItems(Integer userId) {
		return shoppingCartRepository.findOrderItems(userId);
	}

	/**
	 * 商品毎についている削除ボタンが押されたらデータを削除します
	 * 
	 * @param id
	 */
	public void delete(Integer id) {
		shoppingCartRepository.delete(id);
	}

	/**
	 * ショッピングカートに入っている商品ごとの小計を計算しています
	 * 
	 * @param itemPrice
	 * @param quantity
	 * @return
	 */
	public Integer multi(Integer itemPrice, Integer quantity) {
		return itemPrice * quantity;
	}

	/**
	 * ショッピングカートに入っている商品のすべての合計金額を計算しています
	 * 
	 * @param itemList
	 * @return
	 */
	public Integer getTotal(List<Items> itemList) {
		Integer total = 0;
		for (Items item : itemList) {
			total += item.getItemTotal();
		}
		return total;
	}

	/**
	 * 消費税込みの金額を計算します
	 * 
	 * @param totalPrice
	 * @return
	 */
	public Integer taxTotal(Integer totalPrice) {
		return (int) (totalPrice * 1.08);
	}

	/**
	 * 
	 * @param itemId
	 * @return
	 */
	public ShoppingCart find(Integer itemId, Integer userId) {
		return shoppingCartRepository.find(itemId, userId);
	}

	/**
	 * BuyStatusを更新します。 片桐 追加
	 * 
	 * @param status
	 * @param orderId
	 */
	public void updateBuyStatus(Integer buyStatus, Integer id) {
		shoppingCartRepository.updateBuyStatus(buyStatus, id);
	}

	/**
	 * order_idを更新します。片桐追加
	 * 
	 * @param orderId
	 * @param id
	 */
	public void updateOrderId(Integer orderId, Integer id) {
		shoppingCartRepository.updateOrderId(orderId, id);
	}

	/**
	 * カートに既に入っている商品をユーザーが再度カートに追加したとき数量だけ変更します
	 * 
	 * @param quantity
	 * @param itemId
	 */
	public void plus(Integer quantity, Integer itemId, Integer userId) {
		Integer totalPrice = itemsRepository.loadOne(itemId).getPrice()
				* (shoppingCartRepository.find(itemId, userId).getQuantity() + quantity);
		shoppingCartRepository.plus(quantity, itemId, totalPrice, userId);
	}

	/**
	 * 
	 * @param cartList
	 * @return
	 */
	public Integer getTotalPrice(List<ShoppingCart> cartList) {
		Integer totalPrice = 0;
		for (ShoppingCart cart : cartList) {
			Items items = itemsRepository.loadOne(cart.getItemId());
			cart.add(items);
			totalPrice += cart.getTotalPrice();
		}
		return totalPrice;
	}
	
	public List<ShoppingCart> findByOrderIdAndStatusBuyed(Integer orderId){
		return shoppingCartRepository.findByOrderIdAndStatusBuyed(orderId);
	}
	
	public List<ShoppingCart> findAllBuyedStatus(){
		return shoppingCartRepository.findAllBuyedStatus();
	}

}
