package jp.co.rakus.ecommers.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;

import jp.co.rakus.ecommers.domain.Review;
import jp.co.rakus.ecommers.domain.User;
import jp.co.rakus.ecommers.repository.ReviewRepository;
import jp.co.rakus.ecommers.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	ReviewRepository reviewRepository;
	
	public User load(Integer id){
		return userRepository.load(id);
	}
	
	/**
	 *  アドレスがすでに使われているか数を調べる
	 * 
	 * @param mailAddress
	 * @return
	 */
	public Integer findByMailAddress(String mailAddress){
		return userRepository.findByMailAddress(mailAddress);
	}
	
	/**
	 * DBの暗号化したパスワードとフォーム入力されたパスワードが一致しているか判断
	 * 
	 * @param password
	 * @param address
	 * @return
	 */
	public boolean cheackPassword(String password, String address){
		try{
			StandardPasswordEncoder passwordEncoder = new StandardPasswordEncoder();
			return passwordEncoder.matches(password, userRepository.findByMailAddressGetPassword(address));
		}catch(NullPointerException e){
			return false;
		}
	}
	
	/**
	 * アカウント新規登録
	 * 
	 * @param member
	 * @return
	 */
	public User save(User user){
			return userRepository.save(user);
	}
	
	/**
	 * アカウント情報変更
	 * 
	 * @param user
	 * @return
	 */
	public void update(User user){
		userRepository.update(user);
	}
	
	/**
	 * 退会処理
	 * 
	 * @param id
	 */
	public void delete(Integer id){
		userRepository.delete(id);
	}
	
	/**
	 * 注文済み商品にレビュデータを登録する
	 * 
	 * @param review
	 */
	public void reviewSave(Review review){
		reviewRepository.save(review);
	}
	
	/**
	 * メールアドレスに紐づいたユーザ情報を取得
	 * 
	 * @param address
	 * @return
	 */
	public User findUserByMailAddress(String address){
		return userRepository.findUserByMailAddress(address);
	}
	
	/**
	 * 
	 * 
	 * @param id
	 */
	public void updateLoginDate(Integer id){
		userRepository.updateLoginDate(id);
	}
	
	/**
	 * ユーザーの情報を全取得する
	 * 
	 * @return
	 */
	public List<User> findAll(){
		return userRepository.findAll();
	}

}
