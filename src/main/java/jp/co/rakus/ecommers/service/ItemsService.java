package jp.co.rakus.ecommers.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.Review;
import jp.co.rakus.ecommers.repository.ItemsRepository;
import jp.co.rakus.ecommers.repository.ReviewRepository;

@Service
public class ItemsService {
	@Autowired
	private ItemsRepository itemsRepository;
	@Autowired
	private ReviewRepository reviewRepository;
	
	private final Integer DEFALT_DISPLAY_NUM = 9;
	
	public List<Items> findAll(){
		return itemsRepository.findAll();
	}
	
	public List<Items> findSearch(String name){
		return itemsRepository.findSearch(name);
	}
	
	public Items loadOne(Integer id){
		return itemsRepository.loadOne(id);
	}
	
	/**
	 * レビュー情報を全取得
	 * 
	 * 久住担当
	 * @return
	 */
	public List<Review> reviewFindAll(){
		return reviewRepository.findAll();
	}
	
	
	/**
	 * レビュー情報をidで取得
	 * 
	 * 久住担当
	 * @return
	 */
	public List<Review> findOne(Integer id){
		return reviewRepository.loadOne(id);
	}
	
	/**
	 * すべての商品にレビュー情報を追加
	 * 
	 * @param itemList
	 * @param reviewList
	 * @return
	 */
	public List<Items> addReviewAll(List<Items> itemList, List<Review> reviewList){
		for(Review review : reviewList){
			for(Items item : itemList){				
				if(review.getItemId().intValue() == item.getId().intValue()){
					item.add(review);
				}
			}
		}
		return itemList;
	}
	
	/**
	 * idに紐づく商品のレビュー情報を追加
	 * 
	 * @param item
	 * @param id
	 * @return
	 */
	public Items addReview(Items item, Integer id){
		List<Review> reviewList = reviewRepository.loadOne(id);
		for(Review review : reviewList){
			item.add(review);
		}
		return item;
	}
	
	/**
	 * 商品情報編集
	 * 
	 * @param items
	 */
	public void updateItem(Items items){
		itemsRepository.updateItem(items);
	}
	
	/**
	 * idに紐づく商品情報を削除
	 * 
	 * @param id
	 */
	public void deleteItem(Integer id){
		itemsRepository.deleteItem(id);
	}
	
	/**
	 * 商品情報を追加
	 * 
	 * @param item
	 */
	public void insertItem(Items item){
		itemsRepository.insertItem(item);
	}
	
	/**
	 * 商品情報をすべて削除
	 * 
	 */
	public void deleteAllItem(){
		itemsRepository.deleteAllItem();
	}
	
	/**
	 * 商品情報一括処理
	 * 
	 * @param name
	 * @param price
	 * @param description
	 * @param imgSrcs
	 */
	public void insertAllItem(String[] name, Integer[] price, String[] description, String[] imgSrcs){
		for(int i = 0; i < name.length; i++){
			Items item = new Items();
			item.setName(name[i]);
			item.setPrice(price[i]);
			item.setDescription(description[i]);
			item.setImgCode(imgSrcs[i]);
			itemsRepository.insertItem(item);
		}
	}
	
	/**
	 * 商品情報一括変更
	 * 
	 * @param item
	 */
	public void updateAllItem(Items item){
		itemsRepository.updateAllItem(item);
	}
	
	/**
	 * IDに紐づく商品情報を一括変更
	 * 
	 * @param item
	 * @param id
	 */
	public void updateAllItemById(Items item, Integer[] updateId){
		for(int i = 0; i < updateId.length; i++){
			item.setId(updateId[i]);
			itemsRepository.updateItem(item);
		}
	}
	
	/**
	 * ぺージャの値の商品を取得
	 * 
	 * @param page
	 * @param displayNum
	 * @return
	 */
	public List<Items> findByPage(Integer page, Integer displayNum){
		if(displayNum == null){
			displayNum = DEFALT_DISPLAY_NUM;
		}
		return itemsRepository.findByPage(page, displayNum);
	}
	
	/**
	 * 商品情報をdisplayNumの数だけ取得　※このメソッドはいらないかも
	 * 
	 * @param displayNum
	 * @return
	 */
	public List<Items> loadByDisplayNum(Integer displayNum){
		return itemsRepository.loadByDisplayNum(displayNum);
	}
	
	/**
	 * ぺージャーの値を計算
	 * 
	 * @param displayNum
	 * @return
	 */
	public List<Integer> calcPageNum(Integer displayNum){
		int itemAllNum = itemsRepository.findAll().size();
		List<Integer> pageNum = new ArrayList<Integer>();
		for(int i = 0, j = 1; i < itemAllNum; i = i + displayNum, j++ ){
			pageNum.add(j);
		}
		return pageNum;
	}
	
	public List<String> findSearchName(String name){
		return itemsRepository.findSearchName(name);
	}
}
