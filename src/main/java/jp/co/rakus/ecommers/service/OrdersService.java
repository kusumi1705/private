package jp.co.rakus.ecommers.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.rakus.ecommers.domain.Orders;
import jp.co.rakus.ecommers.repository.OrdersRepository;

@Service
public class OrdersService {

	@Autowired
	private OrdersRepository ordersRepository;

	// public List<Orders> ordersCheckList() {
	// return ordersRepository.ordersCheckList();
	// }

	public List<Orders> findHistory(Integer userId) {
		return ordersRepository.findHistory(userId);
	}

	/*
	 * public void updateOrdersTable(Orders orders) {
	 * ordersRepository.updateOrdersTable(orders); }
	 */

	/*
	 * public void updateStatus(Integer userId, Integer status) {
	 * ordersRepository.updateStatus(userId, status); }
	 */

	public void addOrders(Orders order) {
		ordersRepository.addOrders(order);
	}

	public Integer idLoad() {
		return ordersRepository.idLoad();
	}
	
	public List<Orders> findAllHistory(){
		return ordersRepository.findAllHistory();
	}
}
