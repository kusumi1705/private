package jp.co.rakus.ecommers.service;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import jp.co.rakus.ecommers.domain.LoginUser;
import lombok.Data;

@Data
public class LoginUserDetails extends org.springframework.security.core.userdetails.User
{
	private final LoginUser loginUser;
	
	public LoginUserDetails(LoginUser loginUser){
		super(loginUser.getUsername(), loginUser.getPassword(), AuthorityUtils.createAuthorityList("ROLE_USER"));
		this.loginUser = loginUser;
	}
	
	
}
