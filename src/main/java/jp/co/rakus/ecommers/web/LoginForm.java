package jp.co.rakus.ecommers.web;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LoginForm {

	private String email;
	private String password;
}
