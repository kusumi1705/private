package jp.co.rakus.ecommers.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.rakus.ecommers.domain.ComparatorRank;
import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.LoginUser;
import jp.co.rakus.ecommers.domain.OrderItems;
import jp.co.rakus.ecommers.domain.Rank;
import jp.co.rakus.ecommers.domain.Review;
import jp.co.rakus.ecommers.domain.ShoppingCart;
import jp.co.rakus.ecommers.service.ItemsService;
import jp.co.rakus.ecommers.service.ShoppingCartService;
import jp.co.rakus.ecommers.service.UserService;

@Controller
@RequestMapping(value = "/item")
@SessionAttributes({"user","cartNum", "pageNums", "displayNum", "pageNum"})
public class ItemsController {

	@Autowired
	private ItemsService itemsService;
	@Autowired
	private ShoppingCartService shoppingCartService;
	@Autowired
	private UserService userService;
	
	private final Integer DEFALT_DISPLAY_NUM = 9;
	
	@ModelAttribute
	public ItemsForm setUpForm() {
		return new ItemsForm();
	}

	/**
	 * 情報変更フォーム
	 * 
	 * @return
	 */
	@ModelAttribute
	public UpdateForm setUpUpdateForm() {
		return new UpdateForm();
	}

	/**
	 * 商品情報一覧表示
	 * 
	 * @param loginUser
	 * @param model
	 * @return
	 */
	@RequestMapping("/list")
	public String index(@AuthenticationPrincipal LoginUser loginUser, HttpSession session, Model model) {
		
		//セッションスコープから表示画面数を取り出す
		Integer displayNum = (Integer) session.getAttribute("displayNum");
		
		//表示画面数の値が入っていなければデフォルトの数いれる。
		if(displayNum == null){
			displayNum = DEFALT_DISPLAY_NUM;
		}
				
		//ぺージャの値を計算
		List<Integer> pageNums = itemsService.calcPageNum(displayNum);
		
		//必要な商品情報だけ取得※デフォルトは9個
		List<Items> itemList = itemsService.findByPage(0, displayNum);
		
		//レビュー情報取得
		List<Review> reviewList = itemsService.reviewFindAll();
		
		//商品にレビュー情報を追加
		itemList = itemsService.addReviewAll(itemList, reviewList);
		
		model.addAttribute("itemList", itemList).addAttribute("pageNums", pageNums).addAttribute("pageNum", 1);
		return "items/list";
	}
	
	/**
	 * 商品ランキング画面表示
	 * 
	 * @return
	 */
	@RequestMapping(value = "/rank")
	public String displayRank(){
		return "/items/rank";
	}
	

	/**
	 * 検索処理
	 * 
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping("/search")
	public String search(ItemsForm form, @AuthenticationPrincipal LoginUser loginUser, HttpSession session, Model model) {
		String name = form.getName();
		if(name == ""){
			return index(loginUser, session, model);
		}
		List<Items> itemList = itemsService.findSearch(name);
		model.addAttribute("itemList", itemList);
		return "items/list";
	}

	/**
	 * 商品詳細ページ処理
	 * 
	 * @param id
	 * @param model
	 * @param loginUser
	 * @return
	 */
	@RequestMapping(value = "/refer")
	public String refer(@RequestParam("id") Integer id, HttpSession session, Model model, @AuthenticationPrincipal LoginUser loginUser) {
		try {
			Items item = itemsService.loadOne(id);
			//レビュー情報をitemに追加
			item = itemsService.addReview(item, id);
			model.addAttribute("item", item);
			return "items/refer";
		} catch (Exception e) {
			model.addAttribute("error", "商品のリンクからアクセスしてください");
			return index(loginUser,session, model);
		}
	}
	
	/**
	 * 非同期通信　商品情報取得
	 * 
	 * @return
	 */
	@RequestMapping("/jsonItemsList")
	@ResponseBody
	public List<Items> getJsonItemsList(@RequestParam("page") Integer page, HttpSession session,  Model model) {
		//List<Items> itemList = itemsService.findAll();
		
		Integer displayNum = (Integer) session.getAttribute("displayNum");
		
		if(displayNum == null){
			displayNum = DEFALT_DISPLAY_NUM;
		}
		
		List<Items> itemList = itemsService.findByPage((page - 1) * displayNum , displayNum);
		//レビュー情報取得
		List<Review> reviewList = itemsService.reviewFindAll();
		//商品にレビュー情報を追加
		itemList = itemsService.addReviewAll(itemList, reviewList);
		model.addAttribute("pageNum", page);
		return itemList;
	}
	
	/**
	 * ページャー作成処理
	 * 
	 * @param page
	 * @param session
	 * @return
	 */
	@RequestMapping("/jsonGetPager")
	@ResponseBody
	public Integer[] jsonGetPage(@RequestParam("page") Integer page, HttpSession session) {
		
		Integer displayNum = (Integer) session.getAttribute("displayNum");
		List<Items> itemList = itemsService.findAll();
				
		if(displayNum == null){
			displayNum = DEFALT_DISPLAY_NUM;
		}
		//ぺージャーの最大値を計算
		int pageAllNums = (int)((Math.ceil((double)itemList.size() / displayNum)));
		
		//ページャーを調整
		Integer[] pageNums = new Integer[pageAllNums];
		for(int i = 1; i <= pageNums.length; i++){
			if(page <= i + 2 && page >= i - 2 && pageAllNums - 2 > i){
				pageNums[i - 1] = i;
			}else if(page + 3 == i){
				pageNums[i - 1] = 0;
			}else if(pageAllNums - 2 <= i){
				pageNums[i - 1] = i;
			}		
		}
		
		return pageNums;
	}
	
	/**
	 * 検索フォームのオートコンプリート機能
	 * 
	 * @param name
	 * @return
	 */
    @RequestMapping(value = "/autoComplete", method = RequestMethod.GET)
    @ResponseBody
    public List<String> autoCompleteList(@RequestParam("name") String name) {
    	//nameから始まる商品情報を取得
    	List<String> allItemsName = itemsService.findSearchName(name);
 
        return allItemsName;
    }
    
    /**
     * ランキングの作成処理
     * 
     * @return
     */
    @RequestMapping(value = "/getRankList", method = RequestMethod.GET)
    @ResponseBody
    public List<Rank> getRankList() {
    	//ショッピングカートの全購入済みの情報を取得
    	List<ShoppingCart> shoppingCartList = shoppingCartService.findAllBuyedStatus();
    	
    	//オブジェクトを用意
        List<Rank> rankList = new ArrayList<>();
        List<String> list = new ArrayList<>();
        
        //ショッピングカートの情報から商品の名前を取得し一度listに保存 ※ショッピングカートのテーブルにはitemIdしかない為
        for(ShoppingCart shoppingcart :shoppingCartList){
        	 list.add(itemsService.loadOne(shoppingcart.getItemId()).getName());
        }
        
        //listに入っている商品の名前で重複を除去
        Set<String> set = new HashSet<>(list);
        List<String> list2 = new ArrayList<>(set);
        
        //重複を除去した商品名前list2をrankListに格納,0は注文された数（初期値）
        for(String itemName : list2){
        	rankList.add(new Rank(itemName, 0));
        }
        
        //ショッピングカートにある注文された商品の数をrankListに追加していく
        for(ShoppingCart shoppingCart : shoppingCartList){
            for(Rank rank : rankList){
            	if(rank.getName().equals(itemsService.loadOne(shoppingCart.getItemId()).getName())){
            		rank.setNum(rank.getNum() + shoppingCart.getQuantity());
            	}
            }	
        }
        
        //rankListを商品の購入数が大きい順にソート
        Collections.sort(rankList, new ComparatorRank());
        
        return rankList;
    }
	
	/**
	 * レビュー画面表示
	 * 久住
	 * 
	 * @param itemId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/review")
	public String review(@RequestParam("itemId") Integer itemId, @AuthenticationPrincipal LoginUser loginUser, HttpSession session, Model model){
		try {
			Items item = itemsService.loadOne(itemId);
			item = itemsService.addReview(item, itemId);
			model.addAttribute("item", item);
			return "items/review";
		} catch (Exception e) {
			model.addAttribute("error", "商品のリンクからアクセスしてください");
			return index(loginUser, session, model);
		}
		
	}
	
	/**
	 * ページャーからの商品情報取得
	 * 
	 * @param loginUser
	 * @param page
	 * @param model
	 * @return
	 */
	@RequestMapping("/page")
	public String page(@AuthenticationPrincipal LoginUser loginUser, @RequestParam("page") Integer page, HttpSession session, Model model) {
		
		Integer displayNum = (Integer) session.getAttribute("displayNum");
		if(displayNum == null){
		   displayNum = DEFALT_DISPLAY_NUM;
		}
		
		//ぺージャの値を計算
		List<Integer> pageNums = itemsService.calcPageNum(displayNum);
		
		List<Items> itemListByPage = itemsService.findByPage((page - 1) * displayNum , displayNum);
		
		//レビュー情報取得
		List<Review> reviewList = itemsService.reviewFindAll();
		//商品にレビュー情報を追加
		itemListByPage = itemsService.addReviewAll(itemListByPage, reviewList);
		
		model.addAttribute("itemList", itemListByPage).addAttribute("pageNums", pageNums).addAttribute("pageNum", page);
		return "items/list";
	}
	
	
	@RequestMapping("/diplayItemNum")
	public String displayItemNum(@RequestParam("displayNum") Integer displayNum,  Model model){
		List<Items> itemList = itemsService.loadByDisplayNum(displayNum);
		
		//ぺージャの値を計算
		List<Integer> pageNums = itemsService.calcPageNum(displayNum);
		
		//レビュー情報取得
		List<Review> reviewList = itemsService.reviewFindAll();
		
		//商品にレビュー情報を追加
		itemList = itemsService.addReviewAll(itemList, reviewList);
		
		model.addAttribute("itemList",itemList).addAttribute("pageNums", pageNums).addAttribute("pageNum", 1).addAttribute("displayNum", displayNum);
		return "items/list";
	}
}