package jp.co.rakus.ecommers.web;

import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class ReviewForm {
	private Integer id;
	private Integer userId;
	@NotEmpty(message="タイトルを入力してください")
	private String title;
	@NotEmpty(message="お名前を入力してください")
	private String reviewer;
	private Integer itemId;
	private Date postDate;
	@NotEmpty(message="投稿内容を入力してください")
	private String comment;
	private Integer score;
}
