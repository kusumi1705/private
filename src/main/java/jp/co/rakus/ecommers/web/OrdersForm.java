package jp.co.rakus.ecommers.web;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

@Data
public class OrdersForm {

	@NotBlank(message = "お名前を入力してください")
	private String destinationName;
	@NotBlank(message = "メールアドレスを入力してください")
	@Email(message = "メールアドレスの形式が違います")
	private String destinationEmail;
	@NotBlank(message = "住所を入力してください")
	private String destinationAddress;
	@NotBlank(message = "電話番号を入力してください")
	private String destinationTel;
	private String deliveryDate;
	private String deliveryTime;
	private Integer paymentMethod;

}
