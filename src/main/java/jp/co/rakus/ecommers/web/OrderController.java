package jp.co.rakus.ecommers.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.LoginUser;
import jp.co.rakus.ecommers.domain.Orders;
import jp.co.rakus.ecommers.domain.ShoppingCart;
import jp.co.rakus.ecommers.repository.ShoppingCartRepository;
import jp.co.rakus.ecommers.service.ItemsService;
import jp.co.rakus.ecommers.service.OrdersService;

@Controller
@RequestMapping(value = "/order")
@SessionAttributes({ "total", "itemList", "quantity", "cartNum"})
public class OrderController {

	@Autowired
	private OrdersService ordersService;

	@Autowired
	private ShoppingCartRepository shoppingCartService;

	@Autowired
	private ItemsService itemsService;

	/**
	 * 注文確認画面のフォームを生成します
	 * 
	 * @return
	 */
	@ModelAttribute
	public OrdersForm setUpOrdersForm() {
		return new OrdersForm();
	}

	/**
	 * 注文確認時にログインしていなかったらログインフォームを生成します（いるのか？）
	 * 
	 * @return
	 */
	@ModelAttribute
	public LoginForm setUpRegisterForm() {
		return new LoginForm();
	}

	@ModelAttribute
	public ItemsForm setUpForm() {
		return new ItemsForm();
	}
	/**
	 * 注文確認画面を表示します
	 * 
	 * @param form
	 * @param loginUser
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "ordersCheckList")
	public String ordersCheckList(ShoppingCartForm form, @AuthenticationPrincipal LoginUser loginUser, Model model,
			HttpSession session) {

		if (loginUser == null) { // ログインしていないとき、ログイン画面を表示
			return "/user/login";
		}

		if (loginUser != null) { // ログインした人のショッピングカートの商品一覧を取り出す
			Integer userId = loginUser.getUser().getId(); // ログイン者のID取得
			List<ShoppingCart> shoppingCart = shoppingCartService.findOrderItems(userId); // ログイン者のカート情報取得
			Integer total = 0; // 商品合計金額を初期化
			List<Items> itemList = new ArrayList<Items>();
			for (ShoppingCart cart : shoppingCart) { // ショッピングカートのitem_idから商品情報を1件ずつ取得
				Items item = itemsService.loadOne(cart.getItemId()); // 商品IDをセット
				item.setQuantity(cart.getQuantity()); // Itemsに各商品の数量をセット
				total += cart.getTotalPrice(); // 商品の合計金額を計算
				itemList.add(item); // 商品情報を1件ずつ商品リストに追加
			}
			model.addAttribute("itemList", itemList); // 商品リストをセッションスコープに格納
			model.addAttribute("total", total); // 合計金額をセッションスコープに格納
		}
		return "/order/ordersList"; // 注文確認画面へ遷移
	}

	/**
	 * 注文処理を実行します
	 * 
	 * @param form
	 * @param result
	 * @param loginUser
	 * @param model
	 * @param token
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "ordersFinish")
	public String ordersRegistory(@Validated OrdersForm form, BindingResult result, Model model, String token,
			@AuthenticationPrincipal LoginUser loginUser, ShoppingCartForm shopForm, HttpSession session)
			throws ParseException {

		boolean errorFlg = false;

		// バリデーションチェック
		if (result.hasErrors()) {
			errorFlg = true;
		}

		// 配達日入力チェック
		if (form.getDeliveryDate().equals("")) {
			result.addError(new FieldError("OrdersForm", "deliveryDate", "配達日を入力してください"));
			errorFlg = true;
		}

		// 配達時間入力チェック
		try {
			if (form.getDeliveryTime().equals("")) {
			}
		} catch (NullPointerException ex) {
			result.addError(new FieldError("OrdersForm", "deliveryTime", "配達時間を入力してください"));
			errorFlg = true;
		}

		// クレジットカードエラーチェック （支払い方法：2、トークン：null）
		if (form.getPaymentMethod() == 2 && token.equals("")) {
			result.addError(new FieldError("OrdersForm", "paymentMethod", "カード情報を入力してください"));
			errorFlg = true;
		}

		if (errorFlg) {
			return ordersCheckList(shopForm, loginUser, model, session);
		}

		Integer userId = loginUser.getUser().getId(); // ログイン者のuserIdを取得
		List<ShoppingCart> shoppingCart = shoppingCartService.findOrderItems(userId); // ログイン者のカート情報取得
		Orders order = new Orders();
		BeanUtils.copyProperties(form, order); // フォームから送られた値をordersクラスにコピー
		for (ShoppingCart cart : shoppingCart) { // orderItemsリストからordersテーブルに1件ずつ追加
			order.setUserId(userId); // ユーザーIDをセット
			order.setTotalPrice(cart.getTotalPrice()); // 合計金額をセット
			order.setOrderItemsId(userId); // order_items_idをセット

			// ordersに配達日程を登録する
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String date = form.getDeliveryDate(); // 文字列として日付を取得
			String time = form.getDeliveryTime(); // 文字列として時間を取得
			String datetime = (date + " " + time); // 文字列同氏で結合
			Date deliveryDateTime = simpleDateFormat.parse(datetime);
			order.setDeliveryTime(deliveryDateTime);

			// クレジットカード決済処理
			order.setPaymentMethod(form.getPaymentMethod());
			if (order.getPaymentMethod() == 2 && !(token.equals(null))) {
				order.setStatus(3);
				System.out.println(token); // クレジット決済完了を確認するためtokenを出力
			} else {
				order.setStatus(1);
			}
			ordersService.addOrders(order); // ordersテーブルに追加
	
			// ショッピングカートのbuy_statusを変更
			// ショッピングカートのorder_idを変更
			Integer buyStatus = 1; // 購入後のBuyStatus：0 → 1
			Integer orderId = ordersService.idLoad();
			try {
				shoppingCartService.updateBuyStatus(buyStatus, cart.getId());
				shoppingCartService.updateOrderId(orderId, cart.getId());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			//ショッピングカートの中身の数をゼロに変更しヘッダーに表示
			model.addAttribute("cartNum", 0);
			
		}
		return "/order/orderFinish";
	}

	/**
	 * 注文履歴処理
	 * 
	 * @param loginUser
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/history")
	public String orderHistory(@AuthenticationPrincipal LoginUser loginUser, Model model) {
		//ショッピングカートの中身の数をヘッダーに表示
		List<ShoppingCart> list = shoppingCartService.findOrderItems(loginUser.getUser().getId());
		model.addAttribute("cartNum", list.size());

		//ログインユーザの注文情報を取得
		List<Orders> ordersList = ordersService.findHistory(loginUser.getUser().getId());
		//List<ShoppingCart> shoppingCartList = shoppingCartService.findByOrderIdAndStatusBuyed(loginUser.getUser().getId());
		//注文情報に商品情報を追加
		for(Orders order : ordersList){
			ShoppingCart cart = shoppingCartService.findByOrderId(order.getOrderItemsId());
			order.add(itemsService.loadOne(cart.getItemId()));
		}

		model.addAttribute("ordersList", ordersList);
		return "/order/history";
	}
	
}
