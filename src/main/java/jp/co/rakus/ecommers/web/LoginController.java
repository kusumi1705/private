package jp.co.rakus.ecommers.web;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.LoginUser;
import jp.co.rakus.ecommers.domain.Review;
import jp.co.rakus.ecommers.domain.ShoppingCart;
import jp.co.rakus.ecommers.domain.User;
import jp.co.rakus.ecommers.service.ItemsService;
import jp.co.rakus.ecommers.service.ShoppingCartService;
import jp.co.rakus.ecommers.service.UserService;

@Controller
@SessionAttributes({"user", "userLoginDate", "admin"})
public class LoginController {

	@Autowired
	private UserService userService;
	@Autowired
	private ShoppingCartService shoppingCartService;
	@Autowired
	private ItemsService itemsService;
	
	private final Integer DEFALT_DISPLAY_NUM = 9;
	
	/**
	 * フォームを初期化します. 久住
	 * @return フォーム
	 */
	@ModelAttribute
	public LoginForm setUpForm() {
		return new LoginForm();
	}
	
	@ModelAttribute
	public ItemsForm setUpItemsForm() {
		return new ItemsForm();
	}
	
	@RequestMapping("/user/login")
	public String login(){
		return "/user/login";
	}
	

	/**
	 * ログイン失敗時に呼ばれる　エラーメッセージをスコープに入れ　ログイン画面に戻す
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/user/login-failer")
	public String loginFailer(Model model){
		
		String error = "メールアドレスまたはパスワードが違います。";
		model.addAttribute("error", error);		
		
		return "/user/login";
	}
	
	/**
	 * 
	 * @param loginUser
	 * @param model
	 * @return
	 */
	@RequestMapping("/user/loginSuccess")
	public String loginSuccess(@AuthenticationPrincipal LoginUser loginUser,HttpSession session, Model model){
		//ユーザー情報取得
		User user = userService.load(loginUser.getUser().getId());
		model.addAttribute("user", user);
		//ログイン日時更新
		if(user.getLoginDate() != null){
			model.addAttribute("userLoginDate", user.getLoginDate());
		}
		userService.updateLoginDate(loginUser.getUser().getId());
		
		//管理者であるか判断
		if(user.getRole() == 1){
			model.addAttribute("admin",true);
		}
		
		if(session.getAttribute("itemList") != null){
			List<Items> itemList = (List<Items>) session.getAttribute("itemList");
			Integer userId = userService.findUserByMailAddress(loginUser.getUser().getEmail()).getId();
			for(Items item : itemList){
				shoppingCartService.upDateOrderItems(item.getId(), userId, item.getQuantity(), item.getItemTotal());
			}
		}
		//ショッピングカートの中身の数をスコープに追加
		List<ShoppingCart> list = shoppingCartService.findOrderItems(loginUser.getUser().getId());
		model.addAttribute("cartNum", list.size());
		
		return "redirect:../item/list";
	}
	
	/**
	 * 仮
	 * 
	 * @param loginUser
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("/")
	public String index(@AuthenticationPrincipal LoginUser loginUser, HttpSession session, Model model) {
		
		//セッションスコープから表示画面数を取り出す
		Integer displayNum = (Integer) session.getAttribute("displayNum");
		
		//表示画面数の値が入っていなければデフォルトの数いれる。
		if(displayNum == null){
			displayNum = DEFALT_DISPLAY_NUM;
		}
				
		//ぺージャの値を計算
		List<Integer> pageNums = itemsService.calcPageNum(displayNum);
		
		//必要な商品情報だけ取得※デフォルトは9個
		List<Items> itemList = itemsService.findByPage(0, displayNum);
		
		//レビュー情報取得
		List<Review> reviewList = itemsService.reviewFindAll();
		
		//商品にレビュー情報を追加
		itemList = itemsService.addReviewAll(itemList, reviewList);
		
		model.addAttribute("itemList", itemList).addAttribute("pageNums", pageNums).addAttribute("pageNum", 1);
		return "items/list";
	}
	
}
