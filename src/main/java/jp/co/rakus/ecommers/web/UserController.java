package jp.co.rakus.ecommers.web;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import jp.co.rakus.ecommers.domain.LoginUser;
import jp.co.rakus.ecommers.domain.Review;
import jp.co.rakus.ecommers.domain.ShoppingCart;
import jp.co.rakus.ecommers.domain.User;
import jp.co.rakus.ecommers.service.ShoppingCartService;
import jp.co.rakus.ecommers.service.UserService;

@Controller
@RequestMapping("/user")
@SessionAttributes({"userInfo", "user"})
/**
 * ユーザ情報のコントローラー
 * 
 * @author 久住
 *
 */
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
    private ShoppingCartService shoppingCartService;
	
	/**
	 * 登録フォーム初期化
	 * 
	 * @return
	 */
	@ModelAttribute
	public RegisterForm setUpRegisterForm() {
		return new RegisterForm();
	}
	
	/**
	 *　ログインフォームを初期化.
	 *
	 * @return 
	 */
	@ModelAttribute
	public LoginForm setUpForm() {
		return new LoginForm();
	}
	
	/**
	 * 検索に使うフォームを初期化 
	 * 
	 * @return
	 */
	@ModelAttribute
	public ItemsForm setUpItemsForm() {
		return new ItemsForm();
	}
	
	/**
	 * ユーザー情報変更フォームを初期化
	 * 
	 * @return
	 */
	@ModelAttribute
	public UpdateForm setUpUpdateForm() {
		return new UpdateForm();
	}
	
	/**
	 * レビューフォームを初期化
	 * 
	 * @return
	 */
	@ModelAttribute
	public ReviewForm setUpReviewForm() {
		return new ReviewForm();
	}
	
	/**
	 * ユーザ登録画面表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/register")
	public String registerForm(Model model){
		return "/user/register";
	}
	
	/**
	 *　退会処理画面表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/deleteform")
	public String deleteForm(Model model){
		return "/user/deleteform";
	}
	
	/**
	 * ユーザ情報変更画面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/update")
	public String updateForm(@AuthenticationPrincipal LoginUser loginUser, Model model){
		//ログインしているユーザの情報を取得
		User userInfo = userService.load(loginUser.getUser().getId());
		//スコープにユーザー情報を追加
		model.addAttribute("userInfo", userInfo);
		return "/user/update";
	}
	
	
	/**
	 * レビュー画面表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/review")
	public String reviewForm(@RequestParam("itemId") Integer itemId,@AuthenticationPrincipal LoginUser loginUser, Model model){
		//ショッピングカートの中身の数をヘッダーに表示
		List<ShoppingCart> list = shoppingCartService.findOrderItems(loginUser.getUser().getId());
		model.addAttribute("cartNum", list.size());   //ショッピングカートの中身の数をスコープに追加
		model.addAttribute("itemId", itemId);         //商品のIDをスコープに追加
		return "/user/review";
	}
	
	/**
	 * ユーザ登録後の画面表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/registerAfter")
	public String registerAfter(HttpSession session, Model model){
		return "/user/registerAfter";
	}
	
	/**
	 * ユーザ　マイページ画面を表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/mypage")
	public String mypage(Model model){
		return "/user/mypage";
	}
		
	/**
	 * ユーザー情報登録処理
	 * 
	 * @param form
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/registerAction")
	public String create(@Validated RegisterForm form, 
			BindingResult result,
			Model model,
			HttpSession session
			) {
		
		// パスワードと確認用バスワードが一致しているか判定
		if(! form.getPassword().equals(form.getConfirmPassword())){
			result.rejectValue("confirmPassword", null , "パスワードと確認用パスワードが一致していません");
		}
		
		// DBのメールアドレスが重複しているか判断　重複していればエラーメッセージを表示
		if(userService.findByMailAddress(form.getEmail()) > 0){
			result.rejectValue("email", null , "このメールアドレスはすでに使われているので使用できません");
		}
		// バリデーションに掛かった場合、登録画面に戻す
		if(result.hasErrors()){
			model.addAttribute("zip", form.getZip11()); //
			model.addAttribute("addr", form.getAddr11());
			return registerForm(model);

		}
		
			
	    User user = new User();
		BeanUtils.copyProperties(form, user);
		user.setAddress(form.getZip11() + form.getAddr11());
		
		userService.save(user);
				
		model.addAttribute("userInfo", user);
		//登録後ログイン画面に移動する用修正
		//二重登録対策でリダイレクト
		//return registerAfter(session, model);
		return "redirect:/user/registerAfter";	
	}
	
	/**
	 * ユーザ情報変更処理
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateAction")
	public String updateAction(@Validated UpdateForm form, BindingResult result, @AuthenticationPrincipal LoginUser loginUser,  Model model){
		User user = new User();
		
		//現在のパスワードが合っているか確認
		if(userService.cheackPassword(form.getPassword(), form.getEmail())){
			BeanUtils.copyProperties(form, user);
			
			if(! form.getNewPassword().equals(form.getConfirmPassword())){
				result.rejectValue("confirmPassword", null , "確認用パスワードとパスワードが間違っています");
			}
			
			if(form.getNewPassword() != ""){
				user.setPassword(form.getNewPassword());
			}
			
		}else{
			result.rejectValue("password", null , "パスワードが間違っています");
		}
		
		
		if(result.hasErrors()){  
			return updateForm(loginUser, model);

		}
		//情報を変更
		userService.update(user);
		
		model.addAttribute("userInfo", user);
		model.addAttribute("user", userService.load(loginUser.getUser().getId()));
		return "redirect:/user/updateAfter";
	}
	
	/**
	 * 退会処理
	 * 
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	public String delete(User user, UpdateForm form,SessionStatus sessionStatus, HttpServletRequest request, Model model){
		userService.delete(form.getId());
		new AntPathRequestMatcher("/user/logout/sessionInvalidate");
		try {
			request.logout();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		sessionStatus.setComplete();
		model.addAttribute("userInfo", null);
		model.addAttribute("user", null);
		return "/user/deleteAfter";
	}
	
	/**
	 * レビュー処理
	 * 
	 * @param form
	 * @param loginUser
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/reviewAction")
	public String review(@Validated ReviewForm form, BindingResult result, @AuthenticationPrincipal LoginUser loginUser, Model model){
		if(result.hasErrors()){  
			return reviewForm(form.getItemId(),loginUser, model);
		}
		
		Review review = new Review();
		BeanUtils.copyProperties(form, review);
		review.setUserId(loginUser.getUser().getId());
		userService.reviewSave(review);
		return "redirect:../item/list";
	}
	
	@RequestMapping(value = "/modal")
	public String modal(Model model){
		return "/user/registerAfter";
	}
	
	@RequestMapping(value="/updateAfter")
	public String updateAfter(Model model){
		return "/user/updateAfter";
	}
	
	
}
