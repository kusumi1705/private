package jp.co.rakus.ecommers.web;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class ItemsForm {
	private Integer id;
	private Integer[] updateId;
	@NotBlank(message="商品名を入力してください")
	private String name;
	private String productCode;
	@Digits(message="値段を入力してください", integer=1000000, fraction = 0)
	private String price;
	@NotBlank(message="商品説明を入力してください")
	private String description;
	private String imgCode;
	private String imgSrc;
	private String[] imgSrcs;
	private MultipartFile file;
}
