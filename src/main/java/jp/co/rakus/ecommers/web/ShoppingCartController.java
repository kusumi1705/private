package jp.co.rakus.ecommers.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.LoginUser;
import jp.co.rakus.ecommers.domain.ShoppingCart;
import jp.co.rakus.ecommers.service.ShoppingCartService;

/**
 * ショッピングカートコントローラー
 * 
 * @author 鳥屋部
 *
 */
@Controller
@RequestMapping(value = "/cart")
@SessionAttributes({ "total", "itemList", "taxTotal", "cartNum" })
public class ShoppingCartController {

	@Autowired
	private ShoppingCartService shoppingCartService;

	@ModelAttribute
	public ShoppingCartForm setUpForm() {
		return new ShoppingCartForm();
	}

	@ModelAttribute
	public ItemsForm setUpItemsForm() {
		return new ItemsForm();
	}
	/**
	 * 商品一覧画面を表示しています
	 * 
	 * @param loginUser
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/list")
	public String index(@AuthenticationPrincipal LoginUser loginUser, Model model) {

		// ログインしているときの処理
		if (loginUser != null) {
			int userId = loginUser.getUser().getId();
			List<ShoppingCart> cartList = shoppingCartService.findOrderItems(userId);
			Integer totalPrice = shoppingCartService.getTotalPrice(cartList);

			//ショッピングカートの中の商品の数をスコープに追加
			List<ShoppingCart> list = shoppingCartService.findOrderItems(loginUser.getUser().getId());
			model.addAttribute("cartNum", list.size());
			
			Integer taxTotal = shoppingCartService.taxTotal(totalPrice);
			model.addAttribute("taxTotal", taxTotal);
			model.addAttribute("totalPrice", totalPrice);
			model.addAttribute("cartList", cartList);

		}
		return "shopping/cart";
	}

	/**
	 * ショッピングカート処理 ショッピングカートに商品を追加したときの処理
	 * 
	 * @param loginUser
	 * @param form
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/addlist")
	public String addList(@AuthenticationPrincipal LoginUser loginUser, ShoppingCartForm form, Model model,
			RedirectAttributes redirectAttributes,
			HttpSession session) {

		// 共通処理
		Integer itemId = form.getItemId(); // カートに入れられた商品のIDを取り出します
		Integer quantity = form.getQuantity(); // 入力された商品の数量を取り出します
		// Itemsにそれぞれの値をセットします
		Items item = shoppingCartService.loadOne(itemId);// 取り出したIDを元にカートに入れられた商品の情報を取ってきます
	
		Integer itemPrice = item.getPrice();
		Integer itemTotal = shoppingCartService.multi(itemPrice, quantity);// 商品の金額と数量を元に商品ごとのトータル金額を計算します。
		item.setItemTotal(itemTotal);
		item.setQuantity(quantity);
		List<ShoppingCart> cartList = new ArrayList<ShoppingCart>();
		// ログインしている人の処理（DB）
		if (loginUser != null) {
			model.addAttribute("user", loginUser.getUser());

			int userId = loginUser.getUser().getId();// ログインしている人のユーザーIDを取ってきます
			
			if (shoppingCartService.find(itemId, userId) != null) {
				shoppingCartService.plus(quantity, itemId, userId);

			} else {
				// shoppingCartService.updateOrders(userId);//取ってきたユーザーIDを元にordersテーブルに新しくインサートします
				shoppingCartService.upDateOrderItems(itemId, userId, quantity, itemTotal);// order_itemsテーブルにもインサートします
			}

			cartList = shoppingCartService.findOrderItems(userId);// shoppingCart型のリストに、取ってきた情報を入れます
			Integer totalPrice = 0;
			// 上記リストから商品IDを取り出し、そのIDを元に商品情報を取り出し、cartListに商品情報を持たせます
			for (ShoppingCart cart : cartList) {
				Items items = shoppingCartService.loadOne(cart.getItemId());
				cart.add(items);
				for (Items i : cart.getItemsList()) {
					i.calcItemTotal(cart.getQuantity());
				}

				totalPrice += cart.getTotalPrice();

			}
			if (totalPrice != null) {
				Integer taxTotal = shoppingCartService.taxTotal(totalPrice);
				model.addAttribute("taxTotal", taxTotal);
			}
			//ショッピングカートの中身の数をヘッダーに表示
			List<ShoppingCart> list = shoppingCartService.findOrderItems(loginUser.getUser().getId());
			redirectAttributes.addFlashAttribute("cartNum", list.size());
			
			model.addAttribute("totalPrice", totalPrice);
			model.addAttribute("cartList", cartList);
			return "redirect:/cart/list";
		}

		// ログインしていない人の処理（セッション）
		List<Items> itemList = (List<Items>) session.getAttribute("itemList");// スコープに入っているitemListの情報をを取り出します

		// 取り出したリストの中に商品が一つも入っていなければ、新しくリストをnewします。商品があればスルー
		if (itemList == null) {
			itemList = new ArrayList<Items>();
		}

			itemList.add(item);

		Integer total = shoppingCartService.getTotal(itemList); // トータル価格を計算します
		Integer taxTotal = shoppingCartService.taxTotal(total);


		model.addAttribute("taxTotal", taxTotal);
		redirectAttributes.addFlashAttribute("cartNum", itemList.size());
		redirectAttributes.addFlashAttribute("total", total);
		redirectAttributes.addFlashAttribute("itemList", itemList);
		model.addAttribute("itemList", itemList);
		model.addAttribute("total", total);
		return "redirect:/cart/list";
	}

	/**
	 * カートの中の商品ごとの削除ボタンを押された時の処理
	 * 
	 * @param loginUser
	 * @param form
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public String delete(@AuthenticationPrincipal LoginUser loginUser, ShoppingCartForm form, Model model,
			RedirectAttributes redirectAttributes,
			HttpSession session) {

		// ログインしているときの処理（DB）
		if (loginUser != null) {
			model.addAttribute("user", loginUser.getUser());
			shoppingCartService.delete(form.getId());// 取ってきたIDを元に商品情報をDBから削除
			List<ShoppingCart> list = shoppingCartService.findOrderItems(loginUser.getUser().getId());
			redirectAttributes.addFlashAttribute("cartNum", list.size());
			//ショッピングカートの中身の数をスコープに追加
			
			return index(loginUser, model);
		}
		
		// ログインしていない時の処理（セッション）
		Integer itemId = form.getItemId();
		Integer quantity = form.getQuantity();

		// 削除ボタンが押された商品の情報をセットします
		Items item = shoppingCartService.loadOne(itemId);
		Integer itemPrice = item.getPrice();
		Integer itemTotal = shoppingCartService.multi(itemPrice, quantity);
		item.setItemTotal(itemTotal);
		item.setQuantity(quantity);

		List<Items> itemList = (List<Items>) session.getAttribute("itemList");// スコープに入っているitemListの情報をを取り出します
		

		itemList.remove(item); // リストから、商品情報を削除します

		Integer total = shoppingCartService.getTotal(itemList); // トータル価格を計算します


		Integer taxTotal = shoppingCartService.taxTotal(total);

		model.addAttribute("taxTotal", taxTotal);
		model.addAttribute("cartNum", itemList.size());
		model.addAttribute("itemList", itemList);
		model.addAttribute("total", total);
		return "redirect:/cart/list";
	}

	/**
	 * ショッピングカート処理 ショッピングカートに商品を追加したときの処理
	 * 
	 * @param loginUser
	 * @param form
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/change")
	public String changeQuantity(@AuthenticationPrincipal LoginUser loginUser, ShoppingCartForm form, Model model) {

		// 共通処理
		Integer itemId = form.getItemId(); // カートに入れられた商品のIDを取り出します
		Integer quantity = form.getQuantity();
		Integer hiddenquantity = form.getHiddenquantity();
		Integer newQuantity = hiddenquantity - quantity;

		// Itemsにそれぞれの値をセットします
		Items item = shoppingCartService.loadOne(itemId);// 取り出したIDを元にカートに入れられた商品の情報を取ってきます

		Integer itemPrice = item.getPrice();
		Integer itemTotal = shoppingCartService.multi(itemPrice, newQuantity);// 商品の金額と数量を元に商品ごとのトータル金額を計算します。
		item.setItemTotal(itemTotal);
		item.setQuantity(newQuantity);
		List<ShoppingCart> cartList = new ArrayList<ShoppingCart>();

		int userId = loginUser.getUser().getId();// ログインしている人のユーザーIDを取ってきます

		shoppingCartService.plus(newQuantity, itemId, userId);

		cartList = shoppingCartService.findOrderItems(userId);// shoppingCart型のリストに、取ってきた情報を入れます
		Integer totalPrice = 0;
		// 上記リストから商品IDを取り出し、そのIDを元に商品情報を取り出し、cartListに商品情報を持たせます
		for (ShoppingCart cart : cartList) {
			Items items = shoppingCartService.loadOne(cart.getItemId());
			cart.add(items);
			for (Items i : cart.getItemsList()) {
				i.calcItemTotal(cart.getQuantity());
			}

			totalPrice += cart.getTotalPrice();
		}
		if (totalPrice != null) {
			Integer taxTotal = shoppingCartService.taxTotal(totalPrice);
			model.addAttribute("taxTotal", taxTotal);
		}
		model.addAttribute("totalPrice", totalPrice);
		model.addAttribute("cartList", cartList);
		return "redirect:/cart/list";
	}
}
