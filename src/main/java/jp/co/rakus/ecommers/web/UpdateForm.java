package jp.co.rakus.ecommers.web;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class UpdateForm {
	private Integer id;
	@NotBlank(message="名前を入力してください")
	private String name;
	@Email(message="Eメール形式で入力してください　”例　〇〇〇@〇〇.〇〇”")
	@NotBlank(message="メールアドレスを入力してください")
	private String email;
	@NotBlank(message="住所を入力してください")
	private String address;
	@NotBlank(message="電話番号を入力してください")
	private String telephone;
	private String password;
	private String newPassword;
	private String confirmPassword;
}
