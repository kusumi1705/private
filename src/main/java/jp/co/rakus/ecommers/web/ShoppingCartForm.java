package jp.co.rakus.ecommers.web;

import lombok.Data;

@Data
public class ShoppingCartForm {
	private Integer id;
	private Integer orderId;
	private Integer quantity;
	private Integer hiddenquantity;
	private Integer totalPrice;
	private Integer itemId;

}
