package jp.co.rakus.ecommers.web;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class InsertAllItemsForm {
	private String[] name;
	//private String productCode;
	//@Digits(message="値段を入力してください", integer=1000000, fraction = 0)
	private Integer[] price;
	@NotBlank(message="商品説明を入力してください")
	private String[] description;
	private String[] imgSrcs;
}
