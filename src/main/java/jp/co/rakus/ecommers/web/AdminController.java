package jp.co.rakus.ecommers.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.rakus.ecommers.domain.Items;
import jp.co.rakus.ecommers.domain.Orders;
import jp.co.rakus.ecommers.domain.User;
import jp.co.rakus.ecommers.service.AdminService;
import jp.co.rakus.ecommers.service.ItemsService;
import jp.co.rakus.ecommers.service.OrdersService;
import jp.co.rakus.ecommers.service.UserService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private ItemsService itemsService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private OrdersService orderService;
	
	/**
	 * 商品情報のフォームを初期化
	 * @return
	 */
	@ModelAttribute
	public ItemsForm setUpItemsForm() {
		return new ItemsForm();
	}
	
	/**
	 * 商品情報一括追加のフォームを初期化
	 * 
	 * @return
	 */
	@ModelAttribute
	public InsertAllItemsForm setUpAllItemsForm(){
		return new InsertAllItemsForm();
	}

	/**
	 * 管理者のページを表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage")
	public String maypage(Model model){
		return "admin/mypage";
	}
	
	/**
	 * 会員情報変更画面表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateUsers")
	public String userUpdate(Model model){
		model.addAttribute("userList", userService.findAll());
		return "admin/updateUsers";
	}
	
	/**
	 * 商品情報変更画面を表示
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateItem")
	public String updateItem(@RequestParam("id") Integer id, Model model){
		//商品情報を取得してスコープに追加
		model.addAttribute("item", itemsService.loadOne(id));
		return "/admin/updateItem";
	}
	
	/**
	 * 商品情報追加画面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/insertItem")
	public String insertItem(Model model){
		return "/admin/insertItem";
	}
	
	/**
	 * 注文履歴画面表示
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ordersHistory")
	public String ordersHistory(Model model){
		model.addAttribute("orderList", orderService.findAllHistory());
		return "/admin/ordersHistory";
	}
	
	/**
	 * 商品情報削除画面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deleteItem")
	public String deleteItem(Model model){
		model.addAttribute("itemList", itemsService.findAll());
		return "/admin/deleteItem";
	}
	
	/**
	 * 商品情報変更画面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateAllItem")
	public String updateItem(Model model){
		model.addAttribute("itemList", itemsService.findAll());
		return "/admin/updataAllItem";
	}
	
	/**
	 * 商品情報一括追加画面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/insertAllItem")
	public String insertAllItem(Model model){
		return "/admin/insertAllItem";
	}
	
	/**
	 *　会員を退会させる処理
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public String delete(@RequestParam("id") Integer id, RedirectAttributes redirectAttributes, Model model){
		userService.delete(id); //削除処理
		redirectAttributes.addFlashAttribute("adminMessage", "会員情報を1件削除しました");//メッセージをスコープに追加
		return "redirect:../admin/updateUsers";
	}
	
	/**
	 * 商品情報編集処理
	 * 
	 * @param form
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateItemAction")
	public String updateItemAction(ItemsForm form, RedirectAttributes redirectAttributes, Model model){
		Items item = new Items();
		
		//フォームに送信された値をitemに格納
		BeanUtils.copyProperties(form, item);
		item.setPrice(Integer.parseInt(form.getPrice()));
		itemsService.updateItem(item);
		
		redirectAttributes.addFlashAttribute("adminMessage", "商品情報を1件更新しました");
		return "redirect:../item/list";
	}
	
	/**
	 * 商品削除処理
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deleteItemAction")
	public String deleteItemAction(@RequestParam("id") Integer id, RedirectAttributes redirectAttributes, Model model){
		//商品情報を削除
		itemsService.deleteItem(id);
		redirectAttributes.addFlashAttribute("adminMessage", "商品情報を1件削除しました");
		return "redirect:../item/list";
	}
	
	/**
	 * 商品情報追加
	 * 
	 * @param form
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "/insertItemAction")
	public String insertItemAction(
			@Validated ItemsForm form, 
			BindingResult result,
			RedirectAttributes redirectAttributes,
			Model model){
		
		//バリデーションに引っかかれば入力画面に戻す
		if(result.hasErrors()){  
			return insertItem(model);
		}
		
		Items item = new Items();
		
		//フォームに送信された値をitemに格納
		BeanUtils.copyProperties(form, item);
		item.setPrice(Integer.parseInt(form.getPrice()));
		//商品情報追加処理
		itemsService.insertItem(item);
		redirectAttributes.addFlashAttribute("adminMessage", "商品情報を1件追加しました");
		return "redirect:../admin/mypage";
	}
	
	/**
	 * ユーザー情報一括CSVダウンロード機能
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/userCsvDownload")
	public void userCsvDownload(
			HttpServletResponse response){
		//ユーザ情報を取得
		List<User> userList = userService.findAll();
		adminService.DownloadUserCsv(userList, response);
	}
	
	/**
	 * 注文履歴情報一括CSVダウンロード機能
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/ordersCsvDownload")
	public void ordersCsvDownload(
			HttpServletResponse response){
		List<Orders> ordersList = orderService.findAllHistory();
		adminService.DownloadOrdersCsv(ordersList, response);
	}
	
	/**
	 * 商品情報を全件削除
	 * 
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deleteAllItemAction")
	public String deleteAllItemAction(RedirectAttributes redirectAttributes, Model model){
		itemsService.deleteAllItem();
		redirectAttributes.addFlashAttribute("adminMessage", "商品情報を全て削除しました");
		return "redirect:../admin/mypage";
	}
	
	/**
	 * 商品情報一括登録（CSV）
	 * 
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/insertAllItemAction")
	public String insertAllItemAction(ItemsForm form, Model model){
		
		List<Items> itemList = new ArrayList<Items>();
		try {
			 InputStream csvStream = form.getFile().getInputStream();
	         BufferedReader br
	               = new BufferedReader( new InputStreamReader(csvStream, "SJIS") );
	            String line;

	            // ファイルを行単位で読む
	            while( (line = br.readLine()) != null ) {
	                // カンマで分割したString配列を得る
	                String array[] = line.split( "," );
	                // データ数をチェックしたあと代入、プリントする
	                if( array.length != 3 ) throw new NumberFormatException();
	                String name = array[0];
	                Integer price = Integer.parseInt(array[1]);
	                String description = array[2];
	                Items item = new Items();
	                item.setName(name);
	                item.setPrice(price);
	                item.setDescription(description);
	                itemList.add(item);
	                // 内容を出力する
	                //System.out.println( "|"+name+"|"+price+"|"+description+ "|");
	            }
	            br.close();
	        } catch( IOException e )  {
	            System.out.println( "入出力エラーがありました" );
	        } catch( NumberFormatException e )  {
	            System.out.println( "フォーマットエラーがありました" );
	        }
		model.addAttribute("itemList", itemList);
		return "/admin/insertAllItem";
	}
	
	/**
	 * 商品情報一括処理
	 * 
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/insertedAllItemsAction")
	public String insertedAllItemAction(InsertAllItemsForm form, RedirectAttributes redirectAttributes, Model model){
		itemsService.insertAllItem(form.getName(), form.getPrice(), form.getDescription(), form.getImgSrcs());
		redirectAttributes.addFlashAttribute("adminMessage", "商品一括処理を実行しました");
		return "redirect:../admin/mypage";
	}
	
	/**
	 * 商品情報一括処理
	 * 
	 * @param form
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateAllItemAction")
	public String updateAllItemAction(@Validated ItemsForm form, BindingResult result, RedirectAttributes redirectAttributes,  Model model){
		
		if(result.hasErrors()){  
			return updateItem(model);
		}
		
		//商品情報をItemsに格納
		Items item = new Items();
		BeanUtils.copyProperties(form, item);
		item.setPrice(Integer.parseInt(form.getPrice()));
		
		if(form.getUpdateId() != null){
			itemsService.updateAllItemById(item, form.getUpdateId());
		}else{
			itemsService.updateAllItem(item);
		}
		redirectAttributes.addFlashAttribute("adminMessage", "商品一括変更処理を実行しました");
		return "redirect:../admin/mypage";
	}

}
