package jp.co.rakus.ecommers.web;

import lombok.Data;

@Data
public class UserForm {
	private String name;
	private String email;
	private String telephone;
	private String address;
	private String password;
	private String confirmPassword;
}
