package jp.co.rakus.ecommers.web;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

@Data
public class RegisterForm {
	@NotBlank(message="お名前を入力してください")
	private String name;
	@Email(message="Eメール形式で入力してください　”例　〇〇〇@〇〇〇”")
	@NotBlank(message="メールアドレスを入力してください")
	private String email;
	private String address;
	private String zip11;
	@NotBlank(message="住所を入力してください")
	private String addr11;
	@NotBlank(message="電話番号を入力してください")
	private String telephone;
	@Length(min=8,max=20, message="パスワードは8文字以上20文字以内で入力してください")
	private String password;
	private String confirmPassword;
}
